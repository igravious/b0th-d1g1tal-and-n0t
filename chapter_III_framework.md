
\chapter{Coming to Terms}

\epigraph{I moved like a double agent among the big concepts.}
{“England's Difficulty”

(Heaney 1975)}
<!-- pulling my hair out all day trying to figure out how to make citations work in raw_tex -->

# int main()

A decade has passed since the 30^th^ International Ludwig Wittgenstein Symposium was held in Austria in 2007. There, attention was focused on the philosophy of the __information society__ and the __philosophy of information__. Presenter after presenter[^presenter] noted how salient features of the information age were prefigured in Wittgenstein's writings. What is remarkable from this vantage point a decade hence–in light of among other things the dawn of __machine learning__ and the wan morning star of the __semantic web__–is this; how outdated some of those technical observations already are a mere ten years later and how in the wider community those philosophical observations go largely unheeded. It is almost as if we can barely keep up with technological progress nowadays.

[^presenter]: Of the 37 articles collected in the two volumes of the proceedings I wish to draw your attention to only 9 for reasons that will become clear over the course of this chapter:

Meanwhile elsewhere in academia by 2007 __humanities computing__ had already become __digital humanities__ signalling among other things the reaching of a certain technological and digital threshold within the arts and humanities. Philosophers ought to be paying more attention than they have as yet been doing to the lively exchanges in the digital humanities–the Minnesota Press series *Debates in the Digital Humanities*[@goldDebatesDigitalHumanities2012] edited by Matthew K. Gold being a representative example–and of the increasing sophistication of humanities computing projects. Ontologies are no longer the sole purview of the metaphysician; it is not within the confines of a radical __media-philosophy__ that the language of new media is being thoroughly decoded and critiqued. Philosophy is not ‘going digital’ to the same extent that the rest of the humanities are as was imagined it would at the symposium. We need to be asking ourselves why.

Computing and philosophy as a research programme is an unfolding topography. What is attempted here is to bring things better into view, to provide much needed clarity, to bring the terrain into focus. One would think it a simple matter; many years of moving like a double agent has taught me that it very much is not.

This is how I intend to push forward; I grant you that metaphors and imagery about maps, the built environment, and territories may be a touch pedestrian but I find them to be a useful mutual anchor. Let us picture the intersection of __computing and philosophy__ as a foreign or alien city recently discovered and obscured by a confounding xenomorphic haze. Sounding parties have ventured out before us and have returned with tales of a landscape littered with improbable architecture. What this foray attempts is to use the equivalent of telesensing technologies, to methodologically ramp up the resolution, from the conceptual analogue of \textsc{RADAR} to \textsc{LIDAR} to eventually map all the structures of the city in their various stages of construction.

How this is done:

First one surveys the lie of the land to discern and discover the brute features. This is achieved by quickly inspecting the intersection, unpacking terms, probing properties and relations, paying close attention. Concepts at first unclear come into focus. The preliminary features are compared with those causal explanations provided by Aristotle's millennia-old template. As we shall see things differ slightly; we ask why and in doing so rediscover unexpected truths which in this context yield fruitful–for this flâneur at least–insights. Then, with the rough features picked out we propose an attendant typology (a two-by-four matrix of aspects) to serve as our topograph for the city. By next examining every single facet of the intersection of computing and philosophy individual areas of this research programme are verified and placed into the corresponding cell of the conceptual grid. Once we are satisfied that enough ground has been covered as much of the previous literature as possible is then revisited and its efforts are mapped onto the blueprint. The aim of doing this ultimately is to show where I think the efforts of philosophers ought to be most usefully employed in the near future and where perhaps more effort than is strictly necessary has been expended. I am not so lacking in self-awareness that I underestimate the hubris of such an injunction but nevertheless so it is. Finally, conclusions are drawn.

Given that the context here is the 2018 IACAP meet-up there is an opportunity to ask ourselves again what is it we mean by “computing and philosophy”. Go back to a time before the information age, go back before Wittgenstein–consider how philosophy treats politics, we either speak of the philosophy of politics or political philosophy, we speak of moral philosophy, philosophy of science, of mind, epistemology, ethics/aesthetics, metaphysics and so on down the line. You will observe that we never speak of some subject and philosophy. Yet this is how we address this intersection. This points to a perhaps semi-deliberate ambiguity, computing and philosophy covers the philosophy *of* computing as well as philosophy *using* computers. Thematically from IACAP proceedings we can see that this is how the conjunction has been treated.

It has been pointed out in the debates within the digital humanities that the term digital humanities covers both thinking about computers in a humanistic context (a humanities of the digital, if you will) and undertaking academic humanism with computational methods (ergo, humanities by or using computers). Given that there has long been an accepted term for doing humanities digitally–that is to say the aforementioned humanities computing–some scholars argued at the time against the brand change. But it is undoubtedly true that digital humanities casts its net wider than humanities computing. To use the jargon of the digital humanities its acts are performed in a bigger tent[^tent].

[^tent]: From an influential article “'Big Tent Digital Humanities,' a View From the Edge, Part 1” [@pannapackerBigTentDigital2011] in the *Chronicle of Higher Education*.

The term humanities computing was back-formed from the term __scientific computing__–this makes sense because there is no such thing as digital science (the term would be laughable) and it is only because of the illusory incompatibility of the terms digital and humanities that it became necessary to emphasize the compatibility[^compat]. By analogy I propose we fix on the term __digital philosophy__ to cover both philosophy of the digital (and by extension: computation, information, and their cognates) and philosophy by or using digital technology (ditto). Again by analogy philosophy using computers ought to be termed __philosophic computing__.

[^compat]: Time will tell if the designation will wither away once digital humanities has become entrenched. I delve into this illusory incompatibility elsewhere.

Let's tabulate this for clarity.

   1          2                           3                           4
   ------     -------------------------   ------------------------    --------------------
   of         philosophy of the digital   __digital culture__         informatics
   by         philosophic computing       humanities computing        scientific computing
   $\Sigma$   digital philosophy          digital arts & humanities   (digital) science

Table: The of/by dichotomy tabulated.

The cells in that table are terse: philosophy of the digital is an umbrella term for philosophy of computing and also philosophy of information; digital culture is short-hand for looking at digital culture and networked society from a humanistic perspective; informatics covers computer science / informatics itself / software engineering / …

That little word ‘by’ (‘using’) demands a closer inspection. An oft quoted axiom in DH-land is that a mere medievalist with a blog makes not a digital humanist[^medievalist]. What this refers to is, I propose, the following delineation; what it means is that the digital humanities are unconcerned with a certain sort of computational method. The difference is, in fact, one of __computer-mediated communication__[^cmc] of philosophy versus computer-aided philosophy. Philosophic computing is __computer-aided philosophy__. If we accept the relatively unproblematic shorthand, conceptual analysis, for philosophy we get the construction __computer-aided conceptual analysis__ which sports a rather unfortunate acronym which suggests the less unfortunate alternative __computer-aided knowledge engineering__–this is where philosophy needs to go.

Aplogies for such terminologically heavy terrain, luckily we need only familiarise ourselves with the terms once.

[^medievalist]: A quip: I am unable to track down to its source.

[^cmc]:  For precise terminology see *Asis&t Thesaurus of Information Science, Technology and Librarianship*[@redmond-nealAsisThesaurusInformation2005]. Asis&t is the American Society for Information Science and Technology.

We are talking about computational methods, thus we are talking about philosophical methods, about how philosophy gets done, we are talking about philosophy as an activity. So in this context if we speak of a philosopher using a computer it means a certain way of doing philosophy with a computer that goes beyond mere mediation. Let us reserve the little word ‘with’ to refer to the digital as a medium.

   1          2                           3                            4
   ------     -------------------------   ------------------------     --------------------
   of         philosophy of the digital   __digital culture__          informatics
   by         philosophic computing       humanities computing         scientific computing
   with       c-m-c of philosophy         c-m-c of arts & humanities   c-m-c of the sciences
   $\Sigma$   digital philosophy          digital arts & humanities    (digital) science

Table: The of/by/with trichotomy tabulated.

Again, rather terse for the sake of visual clarity. c-m-c stands for computer-mediated-communication.

Putting gingerly to one side the question why or what for philosophy gets done in the first place let us consider the IACAP's remit in light of what philosophy concerns itself. The computational turn or better yet the digital turn allows: [1–of] philosophy as a discipline to rethink existing subject matter and forms of philosophy anew while simultaneously allowing entirely new sub-disciplines and ways of framing to be brought into being; [2–by] philosophers to do philosophy algorithmically whatever that turns out to be; [3–with] the activity or discipline of philosophy to explore via new media. [4–for] a revaluation of the purpose or goal of philosophy–this is noted but not explored here.

   1          2                           3                              4
   ------     -------------------------   ------------------------       --------------------
   of         philosophy of the digital   __digital culture__            informatics
   by         philosophic computing       humanities computing           scientific computing
   with       c-m-c of philosophy         c-m-c of arts & humanities     c-m-c of the sciences
   for        purpose of philosophy       purpose of arts & humanities   purpose of the sciences
   $\Sigma$   digital philosophy          digital arts & humanities      (digital) science

Table: the of/by/with/for polychotomy (4th?)

Each part of the of/by/with/for tetrad is distinct but that is not to say that the boundaries are not porous. Each is surrounded by a constellation of cognates–each bleeds into the other as data becomes code and code becomes data, as medium becomes message and message becomes medium, as the final end influences the arrangement and vice versa.

Message has always been recursive, as has method, now with the digital the medium becomes as recursive as both method and message. The digital–as language–allows for recursion. This means that form nests within form, medium can be contained within medium, conceptually speaking matter/content nests within matter/content.

It is right and proper that equal emphasis is placed within academic philosophy on each facet of the tetrad. The [1..4] which follow echo those above and are arranged in the same order. Each facet must be theorised and is generally entirely discipline independent barring content which is site specific and context sensitive.

[1] It is clear that academic philosophy embedded as it is in the mindset of the written word (so-called logocentrism) is having a harder time coming to terms with this mediatic inflection than other disciplines. A sampling of the theorists who have explored and analysed literary and poetic forms are: (Aarseth 1997) (Di Rosario 2011) (Manovich 2002) (Hayles 2002). Let us call them cybertext theorists, and insofar as academic humanism is oriented around natural language their work applies across the board. Content-wise each field and discipline develops evaluates the impact of the digital on itself so the theorists are too diverse to list.

[2] How humanities disciplines must meet the challenge of computational methods is explored and analysed by, among many others: (Unsworth 2010) (McCarty 2014). For philosophy in particular there is a growing body of work, see for instance: [Computational Philosophy @ PhilPapers](https://philpapers.org/browse/computational-philosophy) and a new (at the time of writing) article here: [Computational Philosophy @ SEP](https://plato.stanford.edu/entries/computational-philosophy/). Let us call them method theorists.

[3] In wider culture and the rest of academia there are certainly an embarrassment of new/hyper/…-media theorists (Ess, Manovich, countless others) but within philosophy there are less so. I have already mentioned (Schmidt 2008) included in the Wittgenstein Symposium, preceding this by three years and the first piece of advocacy for a thorough going media-philosophy in English that I have found is (Sandbothe 2005). Rewinding most everyone knows that the idea of the web reverberated in intellectual circles some time before its birth proper in CERN, there is of course (Mcluhan 1996) 5 and his global village and later global theatre, the Memex of (Bush 1945) and the hypertext of (Nelson 1993) given a full bodied philosophical treatment in Media Manifestos (Debray 1996).

[4] It is a truism that form follows function. Given that the digital radically reconfigures form as demonstrated by theorists above is function similarly reconfigured?

It can be readily seen that this brisk analysis gives rise to the following m-tetrad: message, medium, method, motive.

If these are familiar it is of course because they echo Aristotle's four-fold decomposition (Aristotle 1928) of epistemic explanations into causes. Supposing message is expanded to form and content and if medium is dropped we retrieve Aristotle's tetrad. Most familiar to the digital humanist is that Heidegger invoked exactly such an orthodox analysis[@heideggerQuestionConcerningTechnology2010] but this time regarding the question concerning technology–what is different here is that rather than applying the technique to a known but problematic abstractum we apply the technique to a novel intersection of abstracta. This is because of the differentiation between knowing that, fact, and knowing how, cause. We want to know how to proceed in this alien city.

Let us say we do not discard medium and we do take into consideration both form and content. This is sensible because any discussion of the impact of computing on philosophy must surely take into account the transition from traditional media to multimedia and hypermedia and beyond; similarly we do a disservice to philosophy as an art-form if we concentrate only on how the subject matter of philosophy is impacted by digital technologies and not its form.

In contrast to this plethora of literary forms, philosophical expression in contemporary Anglo-American philosophy reveals a striking homogeneity: the now stock philosophical treatise or paper, common to undergraduates and professional philosophers alike, today occupies an unquestioned position of hegemony in current academic philosophy.[@stewartUnityContentForm2013]

How do we to explain that we have an additional explanatory facet to our questioning why? One clue here is the epistemological tussle that final cause has stimulated down through the centuries with respect to natural kinds of beings and things as opposed to man-made artefacts. The nature of this misalignment and the answer to this puzzle did not immediately present itself to me, it is troubling if the results of one's analysis differ from that of Aristotle. The difference comes down to our idea of space and to the natures of the containers of things we can ask why of, in this case signs bifurcating into signifier and signified.

Notice one assumption the four-causes decomposition makes. It assumes we can question the ways of nature (the world in its entirety) in more or less the same way that we can question the ways of people. We can ask why of a juniper bush as unproblematically as a bronze statue. For Aristotle the teleological nature or cause of a juniper bush is as unproblematic as that of a bronze statue or silver chalice even though the manner in which they have been brought forth to use Heideggerian terminology is radically different.

Nevertheless the classical divisions remain four-fold because both bush and statue are physical objects–each having a material cause, building blocks that are reducible to unformed matter; each having a formal cause, spatio-temporal dimensionality and an inherent ordering that give arise to form and structure; each having an efficient cause, relating to an agent or agency that triggers and participates in the creation; and each having (in Aristotle's reading at least) a final cause, an end or goal it is likely to serve or reach or aim towards.

Let us create a bijective map from the classical to the conceptual. This can be seen to respect both the physis/logos demarcation and the physis/nomos one.

# double figure()

material $≈$ medium6

Notice that an artist also calls the materials by which they use to communicate their artwork (their message) their medium although the medium is also the categorization of art based on the materials used–for example, sculpture, painting, “through the medium of interpretive dance”, and so on. Recall the function of the object label or caption in the gallery or museum. The latter use of medium is more akin to the way we talk of language as a medium and speak of the poetics of space, the language of cinema or the language of new media.

formal $≈$ message → form & content

Semiotics produces a doubling; sign encompasses both sign-vehicle and object (to use Peircean terms7), signifier and signified (to use Saussurean terms). Semiotically this leads to the division of form from content. This point is important and and confused me for a while–content with its echoes of matter and material is maps to the formal cause and not the material cause.

Centuries ago, the Greeks noticed the mystery and tried to develop an appropriate conceptual approach with which to investigate its conditions; but modern Western philosophy, in its logical tradition and preoccupation with linguistic structures, has influenced us to ignore what the Greeks tried to explain. Saint Augustine (C. 397—426) described the mystery this way: ‘A sign is a thing which causes us to think of something beyond the impression the thing itself makes upon the senses’ (Robertson 1958: 2.1.1, 34), We might say, in modern terms, that when we use a medium for communication. It can mysteriously become ‘transparent‘ for us, and so reveal to our thoughts the thing to which it refers. (Keeler 1990)

efficient $≈$ method

Rather than focussing on the craftsperson the idea here is one focusses on the craft. Efficient recalls the etymology of agent and agency so the digital shifts focus to the way in which change is enacted from the actor.

final $≈$ motive

The ends or goals of fields or disciplines is beyond the scope of this paper.

We see the mapping, what does it mean for our notion of space? We speak of the digital realm and we do not think it odd. Surely only monarchs have realms? Monarchs are rulers; who rules our new digital kingdom, princes or principles? Perhaps what unifies our asking why is that we expect reasons to govern the answers, even perhaps for the answers to be rule-based. We can enquire of any artisan why? We could just as legitimately ask of their art, how? We can enquire of all persons why. Aristotle treats the entire world the same way epistemologically speaking.

And what about fictional words? This is what makes humans different according to Yuval Noah Harari: the examples he gives in Homo Sapiens (Harari 2015) are religious fables, narratives, stories and myths, social entities like money, nations and corporations–fictional entities all. The fantasy worlds of Harry Potter and the Hobbits are completely make-believe yet we have no problem asking question after question about the characters and the worlds they inhabit. So too the rich interior worlds we generate on a moment to moment basis and call self. The same applies to that information-bearing space we call the digital realm; it exists as independently as the entire natural world–indeed, this may be its true significance and why it is so unprecedented and life-altering even in our age of ever increasing novelty.

This suggests causes are dependent on the world in question. When asking of the natural or artificial worlds one set or another of causes is in operation, when of conceptual space another still, of fictional worlds yet another. When it comes to explanations one size does not fit all.

Recall the four classical causes:

- material  = what from (substance/materiality/medium/…)
- formal    = what like (shape/dimensionality/…)
- efficient = what by (agent/agency/act/craft/…)
- final     = what for (goal/end/purpose/…)

Let us reflect on these divisions. First, they apply to physical artefacts not conceptual ones. Conceptual objects (howsoever conceived8) have no spatio-temporal dimensionality so the only way in which concepts have form is in the figurative sense. Every time a poet waxes lyrical about “forms of expression” they do so figuratively. Likewise concepts cannot be said to have substance therefore they do not have materiality and yet we speak of the subject matter or content of a text, we speak as if they do. Remember whenever we do, we do so figuratively. Witness how Linda Wetzel speaks about the type/token/mark distinction,
As should be clear from the preceding discussion, types have or are capable of having instances, of being exemplified; they are repeatable. To many, this is enough to count as universals. With respect to being abstract and lacking a spatio-temporal location, types are also akin to universals—that is, they are if universals are. On certain views of types and universals, types, unlike their instances, are abstract and lack a spatio-temporal location. (Wetzel 2014)

All this feels so natural to us that this metaphorical leap is not immediately obvious and neither are its consequences. As touched on before an artist does not use the term substance to refer to the materiality of their work they use the term medium–and yet again when we say that language is a medium we do so only figuratively.

It matters less to us what agency propels philosophy along and for what purpose; it matters very much what way it is propelled. It matters in the same way that the artist attends to their craft.

Let me be clear on one point. I am not asserting that concepts are absolutely immaterial and formless, not at all–when they are instantiated in mind or on the page or in speech or in a digital machine they very much have materiality and dimensionality. Language as a vehicle for representing the world to us is conceptually immaterial and formless. And conceptually speaking again, substance means what the thought or utterance is about, form concerns its presentation and style, medium its mode of expression. The media by becoming digital have opened up new horizons for the forms of our expressions.

This all speaks to philosophy the individual act but not philosophy the social activity. Philosophy taken as a discipline is impacted to the same extent that other disciplines are. This is why to our three categories of form, content9, and method we must add medium.

Call this the digital tetrad:

- content   = what about
- form      = what like
- method    = what by/way
- medium    = what with/in

Each category can be further divided in two like so–for instance, an existing form can be augmented by computation or an entirely new computational form can come into being and similarly for the other categories. Modified existing subject matter and completely new subject matter. Modified existing forms of expression and completely new forms of expression. Modified existing methods and completely new methods. Modified existing media and completely new media. This gives eight boxes or buckets. Let us call the modified Block A, and the completely new Block B. Keep the following in mind as we fill the buckets.

Knowledge viewed methodologically subdivides into knowledge representation and reckoning. To distinguish representation from reckoning when taking computing and philosophy into consideration we will label computer-aided reckoning as computational philosophy. There is no equivalent term computational humanities10. This is one of the few places that digital philosophy is slightly ahead of digital humanities but only by mere fluke.

When literature and poetry become thoroughly infused with computational features we have what is called electronic literature (sometimes shortened to e-lit) and electronic poetry. Literature with hypertextual features is only minimally electronic literature. E-lit has a dizzying number of new forms of expression, philosophy seen as “branch of literature” in the words of Rorty must also undergo an explosion in form.

(To put the Tractatus in hypertext format for enhanced navigation is to augment a form of expression. The text becomes computer-mediated but is still essentially the Tractatus. Writing a document in LaTeX rather than in Word is slightly more sophisticated document processing but not by much, not enough to be philosophic computing.)

Typology:
    A) Modify existing …
        i. philosophy of mind additionally considers what artificial cognition could mean, metaphysics and philosophy of biology additionally consider what artificial life could mean, metaphilosophy additionally considers what impact computing has on philosophy, logic morphs into type theory11
        ii. database/directory of philosophers, database/directory of institutions, database of conferences, database of works incl. contents (corpora), index of works excl. contents, digital catalogue of software projects, digital catalogues of new methods, forms, techniques, technologies, standards, …
        iii. proof checkers, what could usefully be thought of as close reading with computational supports
        iv. digital editing and publishing of traditional philosophical material
           primary material: online journals, electronic books
           secondary material: online encyclopedia (SEP, IEP, Wikipedia)
    B) Novel/new …
        i. philosophy of information, media-philosophy, philosophy of computation, computer or digital ethics, formal ontology
        ii. simple forms: controlled vocabularies for philosophy, taxonomies for philosophy (for example wordnets), treebanks for philosophy, ontologies for philosophy
           hybrid forms: electronic texts with significant computational elements, hypertexts (e.g. Wittgenstein's Tractatus laid out as a hypertext), semantic documents, digital archives
        iii. formal systems visualisation, term extraction and correlation analysis of philosophical terms, topic modelling, domain modelling, text mining, computational stylometry
        iv. computer-mediated communication of philosophy: blogging, email, social media, podcasts, computer-assisted instruction of philosophy, computer-assisted collaboration within philosophy: tracking changes, document sharing, co-authoring

Illustrative Examples:
    A) Modify existing …
        i. computer ethics
        ii. PhilPapers (and more recently PhilPeople)
        iii. computational metaphysics
        iv. Stanford Encyclopedia of Philosophy
    B) Novel/new …
        i. philosophy of information
        ii. Philosurfical
        iii. InPho
        iv. Discovery Project

# void ground()

Now that we are satisfied that the typology is robust and accurate it is time to revisit the explorations of previous researchers in light of the broad divisions. Besides the symposium proceedings documented in (Pichler and Hrachovec 2008) and (Hrachovec and Pichler 2008) these intersectional efforts have been collected in a number of different works. In chronological order they are: (Sloman 1978) , (Burkholder 1992) , (Bynum and Moor 1998) , (Grim, Mar, and St. Denis 1998) , (Floridi 1999) , (Moor and Bynum 2002) , (Floridi 2004) and (Allen and Beavers 2011).

Let us take the works in turn and perform a meta-analysis to give us the state of play. We are ignoring here any one-off projects that have as yet been compiled–I recommend the creation of a catalogue of philosophy projects that use innovative software methods in order to be able track the overall progress of the research programme. To the best of my knowledge no such catalogue exists. One could use the DOAP semantic web vocabulary12 as a common language for the catalogue.

Clearly it would be impossible to condense or compress such a huge amount of meta-analysis into a couple of thousand words in a lossless fashion, nevertheless a rapid run-through in light of our typology is salutary. By performing this meta-analysis I hope to show, as has been touched on in the opening paragraphs, that explorations in content have been emphasised over form and likewise medium has been over method. This is unsurprising for a number of reasons: given what is in the average philosopher's skill set or tool kit, tradition and institutional inertia, and lastly lack of imagination. For example, it is far easier to continue to do X but now do X online as opposed to taking the opportunity to reimagine what it means to do X digitally.

Sloman's 1978 originating work about the Computer Revolution in Philosophy has nothing to say about what it would look like to do philosophy using a computer. But this makes sense because things like natural language processing and computational stylometry were in their infancy–posing questions like that would have been ridiculously premature.

Part One: The epistemological outlook is Quinean, which is to say that philosophy is continuous with science (or the sciences if you prefer) in a way that privileges the sciences over the arts and humanities and regards scientific truth as virtually synonymous with truth. This is a fairly unexceptional viewpoint within philosophy and within the scientific community the idea that philosophy is the handmaiden of science is arguably the dominant view.

Present in Sloman's work and that is lacking in later works are metaphilosophical considerations as to how computing and philosophy could join up. To that end he delves into what conceptual analysis actually is, a question that does not get asked in other texts of this (let us call it) genre and thus aligning Sloman's text oddly enough with works in metaphilosophy such as What is Philosophy? (Deleuze and Guattari 1994) and The Philosophy of Philosophy (Williamson 2007).

Part Two: Considerations of mechanisms turns immediately to a discussion on artificial intelligence–so tantalising is the siren song–and then onto numerical reckoning and the considerations of how gross human capacities such as analogical reasoning, intuition, and perception would be impacted by computation. Finally (in what should arguably be a distinct third part but rather is treated as the final chapter of part two) various philosophical problems are analysed in light of computation. It goes unnoticed there that this is a complete non-sequitur.

Notice how ultimately the text clings to how existing topics within philosophy are altered and how new topics emerge and though it considers the philosophical method does nothing with this analysis though it thinks it does.

Burkholder's 1992 compilation Philosophy and the Computer: part one, two, and three may be safely skipped over dealing as they do with more traditional fare: epistemology and philosophy of language, philosophy of mind, logic and philosophy of mathematics. Part four catches our attention, computer-assisted instruction. This is a step towards thinking about how computers may influence the activity and discipline of philosophy rather than its topics. Of the five projects documented here three deal with visual and interactive methods of proof construction, one deals with dialogic reasoning and argument tracking, one with ethical decision-making.

Fast forward six years later to 1998. Two separate compilations, The Digital Phoenix edited by Bynum and Moor and The Philosophical Computer edited by Grim, Mar, and St. Denis appear in the very same year.

Bynum and Moor's collection is broken into two distinct parts: the first dealing with topical considerations or what they call “the impact of computing on philosophical issues”, the second with mediatic considerations or what they call “the impact of computing on professional philosophy”. In detail the first part looks at familiar philosophical concerns like mind, reason, argument, creativity; and branches like ethics, epistemology, science, metaphysics–the novel area of computation is put under the microscope and, of course, artificial intelligence. The second part looks at the web as a medium through which philosophy could be performed, multimedia as a philosophical aid, and resources on the web. Lastly, looked at are three American Philosophical Association Reports based on computer technology related surveys within philosophical research, teaching philosophy, and professional cooperation.

All in all there is one introductory essay and twenty-three essays proper, the majority topical the minority mediatic. You will notice that both Burkholder and Sloman are referenced placing this text firmly within our genre, notice also how metaphilosophical issues are reduced to issues of pedagogy.

Grim, Mar, and St. Denis's work breaks the mould. Behold code in the service of philosophy. Computational methods are used to model problems in game theory, paradoxes in logic, in something they call epistemic dynamics, and cellular automata. Nearly all the results are graphical, one could not imagine performing these computations by hand for very long. The focus is on philosophical modelling and a decent argument is put forward to recast the activity of philosophy in that light and from there yoke it to what they call philosophical computer modelling.

The duo of Moor and Bynum return in 2002 this time with an edited collection titled CyberPhilosophy. are book-ended by two works of Floridi, the first, 1999's Philosophy and Computing, a solo work, and the second a Blackwell Guide, 2004's more cumbersomely titled Philosophy of Computing and Information13.

Let us deal first with Moor and Bynum. This is very much a continuation of their previous work though the philosophical topics are far more novel. Again as before one part is dedicated to the impact of computing on professional philosophy though this time the section is titled communication and computers. Novel topics are computer ethics, the philosophy of information.

Floridi's 1999 work is a kind of solo breakout work. Since Sloman no one other person that I am aware of has attempted to capture in theoretical terms a good deal of the intersection. The feature of the digital that Floridi homes in on is that of information. Few others have done more to establish this as an entirely new branch of philosophy. The motivation to make philosophy thoroughly infused with information and communication technology is admirable and yet we can see how Sloman (though arguably not a philosopher per se) and also Grim, Mar and St. Denis actually tackle what it means to do conceptual analysis or philosophical modelling in the age of the computer.

This shows I believe that philosophy is overly content-oriented. True, questions of method and form do surface from time to time but far far more works tackle that which can be expressed rather than investigating the forms or methods of expression. This partially reflects the idea of what the purpose of philosophy is. Ask most any layperson or fresh-into-the-discipline undergraduate what they think philosophy is for and the search for the meaning of life is never far from their lips whereas the academic philosopher will invariably speak to philosophical modelling or conceptual analysis. It is partly this orientation which makes philosophy an armchair or seminary discipline rather than an experimental or laboratory one–theories dominate practice, forms of expression dominate forms of life.

The implications of this orientation for the intersection of philosophy and computing is that this content-oriented perspective bleeds through to overshadow form and likewise mediatic considerations overshadow method.

That is why Floridi's 2004 edited volume is valuable. It contains the essay “Computational Modeling as a Philosophical Methodology” (Grim 2003). This landmark essay aligns almost perfectly with the notion of modelling as it relates to the humanities that Willard McCarty puts forward in the already referenced Humanities Computing[@mccartyHumanitiesComputing2014] originally published in 2005. Another standout essay is “Ontology” by Barry Smith because it beats a direct path from metaphysics to software engineering. Also the novel topic of digital art is examined though it should be recognised that within the digital humanities the impact of the digital on art has a long pedigree of analysis.

Next the Wittgenstein symposium in 2007. I highlighted what I think are the key works in the first footnote. We notice a further shift to software projects and get a glimpse of the semantic web (Berners-Lee et al. 2001) but these appraisals are still very much in the minority with respect to topical concerns.

Lastly in this meta-analysis review I draw your attention to a special issue of Synthese in 2011 called “Representing Philosophy”. How does one say this delicately? Here we have the establishment beginning to catch up with the implications of the information age. Many authors (Colin Allen, Barry Smith, Michele Pasin & Enrico Motta) in the slim issue we notice have appeared in previous compilations in our list.

There has been no compilation or single author work on the intersection of computing and philosophy in the sense that we are interested in here since 2011 that I am aware of[^aipac_proceedings].

[^aipac_proceedings]: Besides AIPAC proceedings of course.

It should be becoming clear by now that a good deal of the collected works deal with philosophy of X rather than philosophy by X which means that we have rather a lot of epistemic explorations and investigations and rather less technical investigations.

It is said that to be a digital humanist is to be able to code (well contested) or build (less contested). Therefore to be a digital philosopher is to be able to do philosophy digitally, to be able to code or build software systems that tackle philosophical problems. And not just formal or numerical or logical problems–that is territory already covered and besides this is far simpler than natural language processing and understanding. The terms ‘procedural literacy’ (Ramsay 2011) and ‘electracy’ (Ulmer 2003) among others have been suggested. The bare minimum would be a digital literacy that extends beyond using computers as a tool for communication to the waypoint of understanding what knowledge representation means when done digitally and on to familiarity with actually existing humanities computing concepts, methods, tools, and technologies. Case in point, B.A. and M.A. courses in digital humanities (or some close variants like new media studies or digital cultural studies) are springing up everywhere–besides a few notable exceptions how many philosophy undergraduate and postgraduate courses have embraced digital humanities and humanities computing topics?

We must advocate for less writing and more building, this will inevitably constitute a partial rewiring of academic philosophy. Consider these formal explorations: unstructured text to hybrid texts to structured data–this takes in hypertexts, semantic documents, and databases. Alien concepts all to the average philosopher. When computational methods are applied to the form and content of natural language one gets computational stylometry in all its guises in addition to topic and domain modelling. A far-reaching implication is that competing ontologies for philosophy disagree and this fact is why evolving semantic web technologies are becoming so important.

A short term goal is this. Let us do more computer-aided conceptual analysis which is to say computer-aided knowledge engineering for philosophy. I urge that we experiment radically with form, let there be relatively less philosophy of X, relatively less computer-assisted instruction, and relatively less of computer-mediated communications. I suggest we leverage the work being done in experimental philosophy (x-phi) to slightly pivot the discipline of philosophy from the seminary to the col-laboratory.

A medium term goal is this. Locate permanent funding streams for laboratories for humanities computing which focus on philosophical concerns. Throughout Europe, North America and beyond there few labs dedicated to such endeavours–bar the likes of Centre for Digital Philosophy in Canada and Oxford Internet Institute in the the United Kingdom–this is a situation which ought to be remedied.
