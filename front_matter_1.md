---
title: b0th d1g1tal and n0t
subtitle:
author: Anthony Durity
date: August 2020
year: 2020
header-includes: |
  \qualifications{MA}
  \professor{Prof Graham Allen}
  \supervisors{Dr Orla Murphy\\Dr Joel Walmsley\\Prof Brendan Dooley}
  \sponsor{}
  \let\epigraph\oopszero
  \let\flushleftright\oopsone
  \usepackage{epigraph}
  \usepackage{marginnote}
  \usepackage{changepage}
  \setmainfont[Ligatures=TeX]{DejaVu Serif Condensed}
  \setmonofont[Mapping=tex-text]{DejaVu Sans Mono}
keywords: [philosophic computing, computational philosophy, digital philosophy]
documentclass: uccthesis
classoption:
- indented
- dah
- phd
bibliography: phd.bib
link-citations: true
urlcolor: cyan
nocite: |
  @heaneyStations1975
---

<!-- location of documentclass: ~/texmf/tex/latex/ucc/uccthesis.cls -->

\listoffigures

\listoftables

