
\chapter{Formulation}

Wherein is described -- with a degree of transparency and informality such that many an eyebrow will arch -- the process of arriving at and attempting to mark the boundary of the exact research question/topic.

# Academic Writing Has Constraints

Academic writing has constraints. These constraints are various in nature: stylistic, vocal, biographical, epistemological.

## Stylistic

You can't do this. What is meant by *this* that that contraction there and all like it are forbidden. Has the essential sense of the sentence been altered? No, of course not. But the register has been. Too informal, you see. Why is informality *verboten*? Well, this is a serious business don'tcha know. Can't have personalities jutting through the bloodless battlelines of knowledge creation. So bye bye *can't*, hello cannot. Dispense with chattiness as well. That is a big no no. (Case in point here with the streetwise colloquial, “big no no”). Okay, you are a smart cookie. You know this. Why, you ask, are you being told something that you already know? Because how the question/topic of this dissertation was arrived at was by bumbling circuituous shambolic meandering route -- and its telling will not be aborted by *mere* academic constraints.

## Vocal

That “Wherein is …” bit above. That is a nod to the preposterously descriptive chapter and section titles of the likes of Thackeray and company. Voice, style, register -- they are interlinked. Few artistic are embellishments allowed.

## Biographical

This is the meat of it. For my master's disseratation I wanted to combine the digital and philosophy. My early career was not that of academia, it was IT. I returned to academia because I felt there must be more to life. I discovered philosophy but even then it took ten years and two false starts to combine the two. (Many personal traumas and missteps and academic horror stories omitted.)

## Epistemological

No hedges even if you are unsure, heck, *especially* if you are unsure. You must display an air of  certitude. This is 100% the case, that is 100% *not* the case. Ridiculous.

# No, But Seriously

The world is undergoing a marked and profound change the likes of which it has rarely experienced.

![Digital Transformation](figures/digital-transformation-ngram-comparison.png)

<a href="https://tinyurl.com/y56uem6t">Google Books Ngram Viewer</a>

[@michelQuantitativeAnalysisCulture2011]

What is the nature of that change? Though it sounds like the stuff of science fiction it is nothing less than the advent of the age of the electronic brain. It may appear otherwise but I would argue it is not immediately obvious how this change can be tackled from the academic perspective. But first, let us at the very least come to a concensus on a term to refer to this process. Though I have been working in this area for a number of years it has only recently become obvious to me that the term __digital transformation__ (*DX* for short) ought to be the common coin. Approaching this analytically we see that there are three parts to this: the world, the digital, and transformation. What do we mean by the world here? We mean society, surely. What do we mean by the digital? In the introduction to my master's dissertation[@durityMathematicalToolsDigital2012] I showed that surprisingly enough though many tackle aspects of digital change few attempt to provide a succint *defintion*. As with most things, it is trickier than may superficially appear. And finally, what is meant by transformation?

So the not immediately obvious leap is this. In order to think through the process of digital transformation one needs an adequate __theory of society__. This is not immediately obvious because though everyone is a part of society few even recognise that science and the arts have provided us with theories of society -- and certainly none that have gained currency on a par with theories such as *evolution* or *special relativity*. The theory of society which I advocate we should use that of Niklas Luhmann's[@luhmannDifferentiationSociety1982]. Luhmann's theory weaves a number of theoretical strands together, among others: __cybernetics__, __autopoesis__, __systems theory__, __functionalism__. (Each a significant topic in its own right.) Many disagree with Luhman, and the theory has yet to have its experimental moment as the discovery of DNA does to evolution -- but that is not an insurmountable hindrance, the theory of evolution was used successfuly before experimental verification and many a computer science paper begins with *Given P != NP*. (I personally believe that verification for Luhmann will come in the form of advanced agent-based modelling simluations.)

In Luhmann's world there is system and environment. Systems interpenetrate. There are three primary entities: living systems, organistaions, and social systems. What they have in common is they are autonomus communicative units with their own agency. They are not identical but they can be approached and thought of (from the social perspective) in the same way. Living systems are biological language wielding higher-order organisms, that is to say, *us*. Organisations and institutions are the group-oriented building blocks of society. Families, corporations (munipical, public, or private), nations, and so on. And lastly social systems are the functionally differentiated mega-systems that distinguish modern society from pre-modern society. These are the legal and penal system, the system of capital, religious systems, science, art, the mass media, and so on[@moellerLuhmannExplainedSouls2005]. These may appear to be arbitrary distinctions but what distinguishes Luhmann's theory is that it shows that at the heart (for lack of a better word) of each social system there is an axis of coding which gives rise to the system, the legal/illegal distinction of the legal and penal system and the immanent/transcendant distinction of religious systems. Do not get hung up on the details here.

Interestingly (and again, an avenue for further research) Luhmann rejected in black and white that electronic machines[] had little bearing on his theory of society and his analyses thereof. Fine, let us take up where he left off. For instance one could analyse how 

Once you start thinking about the intersection of computing and philosophy a thought will undoubtedly strike you.

Hey __Bucko__.
