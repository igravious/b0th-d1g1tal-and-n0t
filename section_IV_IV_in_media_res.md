
# In Media Res

\label{In Media Res}

## Chapter Outline

## The War Against Cliché

A piece of advice I have long striven to adhere to is that of the literary endeavour being a war against cliché [@amisWarClicheEssays2002]. Mr. Amis …

### Humans Have Trouble Grokking Exponential Patterns

[uh: footnote?]

This is where Typed Semiotics should have gone. Instead I have to (how do I gingerly put this?) extract something spectacular from my *derrière* post haste.

Talk about what could have been? (Not sure. Am I alllowed to do that? Maybe in Future Directions.)

Many works on digital culture and the networked society [for example: cite, cite, and cite] begin by mapping the seas with head-spinning statistics about the rate of penetration of digital technology into our lives. One aspect of the intention of the work in your hands has been to take the road less travelled at every opportunity – to cloak itself in the well-known Shavian adage,

> The reasonable man adapts himself to the world: the unreasonable one persists in trying to adapt the world to himself. Therefore all progress depends on the unreasonable man.

Paradoxically, given the unrelenting pace of digital transformation one would have thought that the unreasonable position was to hew to the status quo. But, so be it.

So one could point to the ever-quicker adoption rates of telecommunication equipment:

  device                 when      speed                       capability
  --------               -------   ----------                  ----------
  commercial telegraphy  1837      70 wpm (once automated)     text
  fixed line telephony   1876      125~150 wpm (avg. speech)   voice
  radio telephony        1946      "                           "
  1g (analog cellular)   1979      2.4 Kbps                    text & voice
  2g (digital cellular)  1991      64 Kbps                     apps & text & voice
  3g "                   2001      1.4 Mbps                    multimedia messging + above
  4g "                   2009      200 Mbps                    streaming video + above
  5g "                   2019      >1 Gps                      ar/vr + above

Even someone seasoned in tracking these phenomena is still taken aback. From fixed line text to voice took 40 years. From fixed line voice to mobile voice using radio telephony took 70 years. Another 30 years to analog cellular. Just over a decade then to digital cellular. And from that point on there arrives a new generation in the digital standard for mobile/cellular telephony roughly every decade. Change is constant. Has anyone ever plotted this bytes per second curve and given their moniker to it à la Moore I wonder?

Speaking of Moore's Law next let us take processor transistor count, rather than consider the progress in tabular form let's use a publicly available graph:

![Transistor count over time](figures/transistor-count-over-time-to-2018.png)

Aug 20th 2019: “Cerebras Systems Unveils 1.2 Trillion Transistor Wafer-Scale Processor for AI”

And what about the global capacity for all those bits and bytes?

Dec 3rd 2018: “IDC: Expect 175 zettabytes of data worldwide by 2025”

And operations per second.

Jun 17th 2019: “TOP500 Becomes a Petaflop Club for Supercomputers”

You get the picture, as I said, tis clichéd. But as some have noted and many have commented on there is a pattern to these adoption and growth rates. The adoption rate follows an S-curve and the growth rate is exponential. This has lead to the conjecture of what has been called the nerd eschaton a.k.a. the Singularity. One such famous proponent (and now a Google fellow) is Ray Kurzweil. But there are many others. He documents these adavances (and many others) in exahausting detail in his titled *The Singularity is Near*[@kurzweilSingularityWhenHumans2005]. An academic nexus is the Singularity Institute. Philosophers have gotten in on the act, witness Bostrom and Superintelligence.

Is humanity about to be overrun by hyper-intelligent inorganic thinking machines? And if so, what does it mean for philosophy?

Such questions (and their answers), no matter how urgent they may be, fall outside of the scope of this work. I will say this – there is a little known work by J.F. Gariépy which has achieved almost zero academic attention – because its author is a white ethno-nationalist – which posits that humans are architecting their own digital replacement. As outrageous and fanciful and outlandish as this may sound I believe this scenario is far more likely than robot armageddon. But as I say, such questions though bordering on the profane and diabolical are outside the scope of this work. The mere fact that they can be raised *at all* is a testament to how transformative is this inflection point in history. All bets, as they say, are off.

Anyway, back to the task at hand. What have I done here? I have inverted the narrative structure of typical works. Usually they lead with the stats to convince you of the importance of the research and investigations. I have not insulted your intelligence by using this tired old trope; no, I have taken for granted that my reader is a smart hip individual who is fully aware that “Software is eating the world”.

Why turn this trick here then?

Because I can't *not* make the argument that philosophy is about to undergo an upheaval the likes of which it hasn't seen since the dawn of the enlightenment. Yes, that profound. If philosophers can argue that not only are we going to be sharing our world with sentient beings but that those beings are going to be unfathomably smarter than we are – and yet you think that the activity of philosophy gets to go unchanged?

DNA: main plot of HHGTG, Electric Monk in DGHDA.

If philosophy is somehow the "search for truth" or the "search for meaning" (let's cast it in its most everyday and colloquial interpretation) then surely many truths must become uncovered in the age of intelligent machines. But what sorts of truths or meanings could those be?

But let's scale it back.

Philosophy has already been altered (for the better) by electronic communications. One could easily make the case that the gatekeeping scholastic dogma of Christinaity was replaced by a more secular priesthood of academic truth-merchants.

We now live in the age of Alain de Botton, Slavoj Žižek, Stefan Molyneux, Sam Harris, Jordan Peterson (among others), Partially Examined Life, Philosophy Bites, the History of Philosophy without any Gaps, countless YouTubers, and in times gone by philosophy television programmes and philosophy documentaries.

What do all these have in common? New media. A return to the oral from the written.

This chapter is doubly clichéd because it recapitulates to some degree the forays into digital philosophy of earlier works. It does so knowingly because it must flesh out one fourth of the architectonic schema laid out at the start of this work whereas others dive in where there inklings and notions take them.

So putting aside forms both hybrid and new, putting aside metaphilosophical concerns, putting aside both computational philosophy and philosophic computing let us summarise what has been said about new media philosophy and let us see if we cannot bring a fresh perspective to the most explored branch of the digital tetrad.

And you know what? It is somewhat tiring to always be striving to be innovative, to push the boundaries, let me have this simple excursion.

### Citing New Media Is Painful

Be it computer program, digital project, TV series, …

## New Media

Not grappled with here are philosophical essayists in the TV medium like say Werner Herzog and um among many others. (sidenote: I break this rule for Adam Curtis.) Neither are the filmic works of philosophical fiction like book adaptations like 1984 (1948 book by Orwell, 1984 adaptation by Michael Radford) or … or original feature length works like Stalker (seminal 1979 work by Tarkovsky) or …

(
    Why not Anto?

    Because they're not philosophy *proper*.

    Lars Iyer / Sinéad – interview

    Search of IMDB topic:
    – Irrational Man (2015) Woody Allen
    – 
)

I want to impress upon us the range deeply philosophical new media ventures.

### TV Documentaries

- The Great Philosophers from 1987 presented by Bryan Magee [@mageeGreatPhilosophers1987]
  This is a series of interviews conducted with notable thinkers about great philosophers, the deliciousness being that some of these thinkers are undoubtedly great philosophers in there own right.
- Of Beauty and Consolation from 2000 presented by Wim Kayzer []
  Less formal the Magee's offering this series of 27 episodes more or less hews to the topic of the consolation of aesthetics. In this case not all the interviewees are philosophical figures, some are musicians, some are artists, some are scientists – of note are Roger Scruton, Martha Nussbaum, and Richard Rorty.
- The Century of the Self from 2002 created and narrated by Adam Curtis [] and All Watched Over by Machines of Loving Grace from 2011
  
### Feature Films

Feature length films about particular philosophers, philosophical schools, or key philosophical topics.

- Agora starring Rachel Weisz
- Žižek! starring as himself
- The Pervert's Guide to Ideology
- …

### Significant Blogs

- Leiter Report
- …

### Podcasts

  Podcast Name                       Creator(s)           Affiliation      Subscriber Count†
  ----------------                   -------------        --------------   -----------------
  Partially Examined Life            Mark Lisenmayer      –                            4,507
  Philosophy Bites                   Nigel Warburton      Oxford Uni                     182
                                                          Freelance
  History of Philosophy              Peter Adamson        LMU, KCL                     5,266
    Without any Gaps
  Making Sense with Same Harris      Sam Harris           –                           50,435
  In Our Time: Philosophy            Melvyn Bragg         BBC Radio 4                  4,694
  The Jordan B. Peterson Podcast     Jordan Peterson      Toronto Uni                    178
  Philosophy? WTF??                  Danny O'Donnell
                                     Dr. Michael Alsford

### On-Demand Streaming Video

- School of Life
- Munk Debates
- Žižek versus Peterson

### MOOCs

- Coursera
- EdX
- Open University

### Social Media

The usual suspects.

## The Implications for Philosophy as a Discipline

### Three patterns

(1) Philosophy *as a profession* has heretofore been a deeply academic profession. Philosophy is not unique in this regard, there are many other theory-oriented or ideology-oriented …

(For the sake of argument here I exclude so-called practical philosophy which shades into religious or spiritual or meditative practice.)

Unless one passes through academia one does not become acquainted with the river of philosophical thought flowing from ancient Greece/China/… and down to the present day. In fact, prior to the internet, unless one went out of one's way to seek out the river one could never swim in it. And how does one seek out that which one has no knowledge of? Hence, religious instiitutions being the primary gatekeepers of philosophical thought for countless centuries.

(2) Philosophy has adapted itself to each and every electronic and digital media. Consequently as new media has proliferated, so has philosophy (along with everything else, i.e. cat videos).

Oh wow.

(3) The push and pull of the Oral versus the Written throughout history as social convention and technology dictate.

As is ceaselessly pointed out by new media theorists a novel feature of new media is its *interactivity*. What this means for philosophy is increased philosophical discourse, back-and-forth, and engagement. This can only be a good thing unless of course one abhors the idea of the proles becoming engaged.

The Socratic versus the Oratic. By oratic (to coin a term) I mean the usual process whereby the Socratic dialogue proceeds internally and on the page and the resultant sculptured and architectonic whole is bequeathed in-toto to the public whether they like it or not. The Grecian Duo exemplify this as they do so often.

A further interesting point of note is that many "proper" philosophers do not consider most of these new media darlings to be actual philosophers. Perhaps there is consternation that the reins of philosophical discourse are being wrestled from them?

The art of rhetoric becomes more important bringing us full circle to Aristotle.

### Advertising and Patronage

Because internet.

> Man does not live on bread alone

Bar tutoring and lecturing how to monetise or subsidise one's philosophising? Get paid to think, get paid to speak, get paid to write.

The internet has made two new revenue streams possible. Online advertising revenue. Peer-to-peer patronage.
