\begin{dedication}
To everyone everywhere.
\end{dedication}

\prelim{Preamble}

This is the preface.

A preface is generally the author recounting the story of how the book came into being, or how the idea for the book was developed. This is often followed by thanks and acknowledgments to people who were helpful to the author during the time of writing. FYI: [Parts of the Book](https://en.wikipedia.org/wiki/Book_design).

Write this last. (And write the introduction second to last.)

Seriously, this is not the first chapter. (It's not really even a chapter, is it?)

\begin{acknowledgements}
Thanks for nothing losers.
\end{acknowledgements}

