
$(info building …)

# make the dissertation, `it' is the default target

# non file targets
.PHONY: dissertation
.PHONY: formatting
.PHONY: show_error
.PHONY: zip

dissertation: appendix.md glossary.md phd.pdf show_error

formatting: formatting.pdf show_error

APPENDIX_LIST := $(shell cat appendix.list)
# $(info … $(APPENDIX_LIST))
GLOSSARY_LIST := $(shell cat glossary.list)
# $(info … $(GLOSSARY_LIST))
SOURCE_LIST   := $(shell cat source.list)
# $(info … $(SOURCE_LIST))
SOURCE_LIST_G := $(shell cat source.list | grep -v glossary)
# $(info … $(SOURCE_LIST_G))
GLOSSY        := $(shell ls glossary/*.gloss)

FORCE_UPDATE  := me.gloss

ifeq (, $(GLOSSY))
$(warning no glossary files, forcing update …)
$(shell cd glossary; touch $(FORCE_UPDATE); cd ..)
GLOSSY := $(shell ls glossary/*.gloss)
else
$(info good to go …)
endif
# $(info … $(GLOSSY))

ifeq (, $(shell which pandoc))
$(error "No pandoc in $(PATH), consider doing `sudo apt install pandoc` and `sudo apt install pandoc-citeproc` or whatever")
endif

zip:
	zip example.zip Makefile appendix.list $(APPENDIX_LIST) source.list $(SOURCE_LIST) figures/*.png phd.pdf

# the appendix is composed of a template and data from Zotero
# it is rebuilt when the builder program is changed and if the
# Zotero data changes
appendix.md: $(APPENDIX_LIST)
	cd digitature; ruby make_digitature.rb; cd ..

# if any of the other sources are newer than the glossary, remake it
# also remake it if the builder program is changed
glossary.md: $(GLOSSARY_LIST) $(SOURCE_LIST_G) $(GLOSSY)
	cd glossary; ruby make_glossary.rb; cd ..

# the dissertation proper is composed of markdown files
# one per chapter, with a special pandoc-aware chapter
# it uses a modified UCC documentclass which is installed locally
# markdown -> latex -> pdf
#
# all the key pandoc options are on the first line,
# then each chapter in turn
# then capture standard output and standard error to separate files
#
# relies on a phd.bib file which holds the citations (which relies on Better BibTeX!)
# this is exported from the appropriate collection in Zotero
# the literature references and actual dissertation references are in distinct collections
# phd.pdf: front_matter.md *body* appendix.md references.md
phd.pdf: phd.bib $(SOURCE_LIST)
	pandoc --verbose --pdf-engine=xelatex --filter=pandoc-citeproc -o phd.pdf --to=latex --from=markdown-latex_macros \
		$(SOURCE_LIST) \
		> xelatex.log 2> xelatex.err

formatting.pdf: formatting/setup.md formatting/formatting.md
	pandoc --verbose --pdf-engine=xelatex --filter=pandoc-citeproc -o formatting.pdf --to=latex --from=markdown-latex_macros \
		formatting/setup.md formatting/formatting.md \
                > xelatex.log 2> xelatex.err

# use - to ignore return code of grep if no match
# use @ to not output command

show_error:
	@grep -v '\[' xelatex.err | true


# useful sites
#
# https://stackoverflow.com/questions/2670130/make-how-to-continue-after-a-command-fails
# https://opensource.com/article/18/8/what-how-makefile

