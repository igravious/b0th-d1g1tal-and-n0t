
\chapter{Code \& Data}

# Twilight of the Analog: How to Philosophize with a Computer

\label{Twilight of the Analog}

\epigraph{[…] new forms of knowledge production that manifest in a shift of scholarly output from the \emph{written} to the \emph{made}}
{(Kräutli and Boyd Davis 2016)}
<!-- “Digital Humanities Research Through Design” -->

## Scholarly Writing versus Scholarly Building

Scholarly writing, broadly construed, as practised within the humanities is a moderately well understood academic craft that has been slowly refined over the centuries. It results in the scholarly text. (We set to one side for the moment the way in which unorthodox presentational forms [[@patelPhEmbraceAlternative2016]; [@viscontiHowCanYou2015]; [@sousanisUnflattening2015]] of digital scholarly communication are beginning to impact the thinking surrounding the scholarly text and constrain our focus here mostly on unorthodox computational forms and unorthodox computational methods.) Scholarly building is, I propose, a nascent craft which combines data wrangling, software design and development -- the stuff of software engineering and data science -- plus humanities scholarship.

It is unusual that any one person can master all these disparate skills. Therefore, unlike scholarly writing which is largely a solitary affair scholarly building is far more often than not a collective effort. This, one presumes, explains the emphasis on collaboration in humanities computing projects and within digital humanities in general. Interminable debates notwithstanding your average digital humanist arguably now accepts that building is an integral part of the trade, and that being the case the discussion should rightly move on from “are you really a digital humanist if you don't build?” to “how and what should we be attempting to build?”. That is why the exegesis of code, data, results, and conclusions in this section is interleaved with the topics of research through design and scholarly building.
Herein specific instances of philosophic computing and data philosophy[^short_for] are considered. Collectively these novel approaches to philosophy are split into two parts. In the first part a few significant and germane examples that exist in the wild are covered – the examples here have been catalogued in the background material along with a few notes, we go into more depth here. As indicated previously the catalogue of digital philosophy software is covered in “Appendix A: Digitature2 Review”. The following examples in the wild provides more context for the then subsequent part which covers what was assembled in-house over the course of digging into the intersection of computing and philosophy.

[^short_for]: Short for data-driven philosophy, an analogue of data science, which gives us data scientist thus likewise we have the construction data philosopher and data philosophy–odd though these novel constructions sound to the ear (initially at least). Lest we forget scientists, after all, are mere natural philosophers with the rather recent designation of scientist back-formed along the lines of artist. Explore the fascinating The Philosophical Breakfast Club[@snyderPhilosophicalBreakfastClub2011] for a deep dive on the topic.

If one is familiar with the work of Kräutli and Boyd Davis then the preceding should make it amply clear why this chapter started with a quote by them from "Digital Humanities Research Through Design". The take home point for philosophers is that anything that can be said of digital humanities in general must ipso facto apply in the specific instance to every discipline in the humanities. They continue:

For Digital Humanities research it is therefore important to recognise the new knowledge that is generated in the process of making, and to make this knowledge explicit. This requires an awareness of the role of design as a research method and the ability to follow a methodologically sound design process.

Rather than focussing on historical projects and datasets as they do let us focus on philosophical ones. In the very near future your average philosopher is going to have to update what sorts of things they hold in their knowledge-base. In addition to their knowing specific philosophers and their work they'll have to about significant philosophical software projects and datasets.

When it comes to discussing building it is more commonplace to quote Steven Ramsay and though I ultimately agree with Ramsay I find him to be a touch too dogmatic. See, by way of example, the oft-cited "On Building"[@ramsayBuilding2011]. At this point I hear the cry of “Et tu, Brute?” from my colleagues.

## In the Wild

The three examples here have been chosen because each highlights a facet of what it means for philosophy to go digital. The first highlights what can be achieved when aspects of reasoning go digital, the second highlights what can be gleaned from the automated analysis of style, the third (and by far the most significant example here, at least in terms of scope and manpower) highlights what it means when aspects of representation get fed into the machine.

## Specimen 1: Computational Metaphysics

From the paper's abstract,

In this paper, the authors describe their initial investigations in computational metaphysics. Our method is to implement axiomatic metaphysics in an automated reasoning system. In this paper, we describe what we have discovered when the theory of abstract objects is implemented in prover9 (a first-order automated reasoning system which is the successor to otter). After reviewing the second-order, axiomatic theory of abstract objects, we show (1) how to represent a fragment of that theory in prover9’s first-order syntax, and (2) how prover9 then finds proofs of interesting theorems of metaphysics, such as that every possible world is maximal. We conclude the paper by discussing some issues for further research. [@fitelsonStepsComputationalMetaphysics2007]

What we have here is computer-aided close reading and proof checking of philosophical arguments. It can be viewed as an obverse of Moretti's distant reading because the human mind is needed to denude the text in question of its materiality and render it into pure symbolic form, where the symbols are mere placeholders for the machine and a human mind is required to recover its materiality. Of course it displays the contemporary logician's thrall to the merely formal as is typical and which is to the detriment of philosophy and beyond.

One can imagine the man-hours involved in lifting arguments from texts, verifying by machine, tweaking them, strengthening them. That is not to say that this is either fruitless or thankless work, rather it is to say that the reason why some texts have become part of the canon is because their arguments have been vetted by a small army of scholars over the years. It is an interesting logical enterprise in itself to subject key texts and key arguments – here, Anselm's famous ontological argument (for the existence of God) – but it's value to future philosophy is open to debate I would say. Of course, once we have machines that can interpret and reason that's a different matter, but as intriguing and thought-provoking as that idea may be that plonks us again firmly within the realm of science fiction [@adamsDirkGentlyHolistic1987].

## Specimen 2: Stylometrics Applied to Philosophical Texts

As is often the case the next example comes about due to a serendipitous encounter rather than tireless research on the behalf of yours truly. During a general workshop on topic modelling by Dr. Koentges at our institution at the request of the digital humanities department the historian let it slip that he had used statistical methods to shed light on the so-called Corpus Platonicum. It had not be known to me before this that the authorial attribution of some of the Platonic dialogues is contested – I should have expected this given our remove. One tends to concentrate on the key dialogues like Gorgias and Phaedrus and The Symposium and, of course, The Republic.

Stylometry is the art of judging a work's stylistic content, often with the aim of attributing authorship to disputed or anonymous works. Contemporary stylometrics relies on computers for statistical analysis. This is done by counting and tracking the distribution and frequency of function words – yes, those again. This computational stylistics places a statistical bound on the scholar's sense of style and augments his or her discernment. This causes similar styled texts to cluster together in statistical space. In the case of Plato and his corpus Koentges has found that the contested works are most likely not by Plato's hand:

One preliminary result is that there is little stylistic evidence that the Menexenus was written by the authorial entity we identify as Plato. [@koentgesComputationalAnalysisCorpus2018]

## Specimen 3: Ontology for Philosophy

Excuse me again while I excerpt at length,

> The application of digital humanities techniques to philosophy is changing the way scholars approach the discipline. This paper seeks to open a discussion about the difficulties, methods, opportunities, and dangers of creating and utilizing a formal representation of the discipline of philosophy. We review our current project, the Indiana Philosophy Ontology (InPhO) project, which uses a combination of automated methods and expert feedback to create a dynamic computational ontology for the discipline of philosophy. We argue that our distributed, expert-based approach to modeling the discipline carries substantial practical and philosophical benefits over alternatives. We also discuss challenges facing our project (and any other similar project) as well as the future directions for digital philosophy afforded by formal modeling. [@bucknerEncyclopediaOntologyDynamic2011]

There are those who reject automated methods for designing ontologies. One vocal opponent is [[@grenonPDCPhilosophyOntology2007]; [@grenonFoundationsOntologyPhilosophy2011]] who push their own manually wrought general ontology which they then have tailored to different fields. It should be immediately obvious that the reason for this is that their world-view is encapsulated by their given ontology, I can think of few other quite as self-defeating advances in knowledge representation. There are opponents to the opponents. Blissfully unaware are the vast majority of philosophers that the digital structures which will shape their field for the next couple of centuries are in the hands of so few.

## In-House

One of the innovative computational methods to analyse natural language texts used in humanities computing is, as has been noted, topic modelling. [@bleiTopicModelingDigital2012]; [@walkowiakBagofWordsBagofTopicsWordtoVec2018]; [cite…]] and similar distant reading[@morettiDistantReading2013]  tools. A key pillar of this dissertation is the proof of concept of these and similar methods to philosophical texts, including texts with a distinct philosophical leaning. Clearly then, the start of such a digital philosophical investigation must begin with a corpus of philosophical texts.

The idea is to treat philosophical texts as data, to treat philosophy as just another branch of literature, a genre of literature. From this perspective, initially at least, there is nothing special about philosophy. Only much later will the philosophical nature of the content become important.

Computational linguistics has been making increasing strides [cite…] in recent years. Lately it is as if not a month goes by without an announcement of some advance or other [cite…]. Though it is imperative, I believe, that philosophers understand these advances and the potential impact for philosophy this is not the place for that discussion.

Imagine three sets of philosophical texts: label them U, P, and C. Scratch that, back up one second. Do we even have a good handle on what is a philosophical text? There is the class of novel and short story which are philosophy dressed up as narrative. One can think of Atlas Shrugged by Rand, Animal Farm by Orwell, the Outsider by Camus, Thus Spake Zarathustra by Nietzsche, The Handmaid's Tale by Atwood, and so on3. (Never non-textual philosophical content such as film, podcasts, …) We see that philosophical themes and considerations can be implicit rather than explicit and moreover can be hidden behind yet another layer when allegory is employed. One can imagine a stupid algorithm concluding that railroads, steel and livestock are very important philosophical topics when in fact the important topics are the individual, collectivism, capitalism, socialism, freedom, tyranny, and the like. It is better then that though we recognise that there are key philosophical texts that ought to be part of any philosophical canon their content is inaccessible to current technology.
As Ayn Rand observed in For the New Intellectual,

I am often asked whether I am primarily a novelist or a philosopher. The answer is: both. In a certain sense, every novelist is a philosopher, because one cannot present a picture of human existence without a philosophical framework; the novelist's only choice is whether that framework is present in his story explicitly or implicitly […]
[@randNewIntellectualPhilosophy1963]

(Really what is being asked is that genre be rendered transparent to the machine. Satire, humour, allegory: to interpret these must require huge advances in computational linguistics one imagines.)

system architecture and overview

The software world suffers from an embarrassment of operating systems, languages, standards, and technologies. The software development world in a similar fashion suffers from a glut of design practices and methodologies. Your author is old enough to have seen Agile and Scrum burst onto the scene, the new hotness for a moment to eventually become as fashionable as bell-bottoms at an underground rave party. Your author remembers when XML was the saviour data transport technology, the new lingua franca of a heterogeneous world of data standards. Ditto Java. After enough experience one hews to what works, not what is fashionable. At the moment for instance, Single Page Applications (SPAs), Isomorphic JavaScript, Front End Frameworks with Data Binding, and Virtual Document Object Models (Virtual DOMs) rule the developer mindshare. Keeping up with the changing trends and tech in tech is almost a full-time job in itself. Hence, collaborative teams, multi-year projects, enormous budgets.

Software design patterns have been developed to slay the dragon of software complexity and surface differences. Once one learns the fundamental data structures (singly or doubly linked list, queues, maps, trees, …), algorithms (searching, sorting, …) then one can learn patterns like The Factory Pattern, or Model-View-Controller, or … whatever.

Ruby on Rails is built around the language Ruby. Ruby itself is one of a class of general purpose interpreted languages, the others being from oldest to youngest: Perl, Python, Ruby, PHP, JavaScript. Apologies, as always, if I've left out the love of your life. There's always a trade off. Here the trade is terseness for speed. C, C++, C#, Java, while all fine languages have a compile cycle and are verbose. They're also blazingly fast in comparison to the interpreted languages just mentioned. What's made a difference is hardware improvements, specifically Moore's Law.

Python is fast becoming the darling of data scientists and computational linguists. As a result one can imagine it becoming required learning in digital humanities departments. Ruby on Rails has carved out a niche for prototyping and building websites, it's a full-stack technology built around a number of design patterns, the two most significant of which are: Active-Record and Model-View-Controller. Active-Record maps relational data structures (entities (including their attributes) and their relations and constraints) onto object-oriented data structures and hierarchies. Model-View-Controller forces what is called a separation of concerns between the front-end (View), the back-end data-store (Model) and that which routes user requests and mediates between them (Controller). PHP powers WordPress, Ruby on Rails allegedly powered an early version of Twitter. Both Zooniverse and FromThePage, two transcription platforms used in digital humanities are built with Ruby on Rails. Omeka, a popular digital archive software is built on PHP. All use JavaScript. Perl is the only interpreted language from the family of five that has next to no traction in humanities computing. The reason? Fashion. Nothing intrinsic to the language – indeed its creator, Larry Wall, is a free-software/open-source visionary and a remarkable essayist. For the record, the others are: Yukihiro Matsumoto (Matz: Ruby), Guido van Rossum (Python), Rasmus Lerdorf (PHP) and the recently maligned Brendan (by a process of elimination: JavaScript).

The architecture revolves around a data-store, a corpus of notable philosophical texts, the textual metadata, and the authors of those texts. Because the long-term goal is for there to be a centre of continuous learning, knowledge production, and experimentation, that there would be a way to track experiments and the writing-up of those experiments. This could easily be extended into a directory or catalogue of digital philosophy software and attendant academic articles. No such thing exists, it is puzzling why it does not.

What information is being tracked here? Philosophical figures from all eras and all cultures. This may sound expansive to the point of impossibility but it turns out that the best structured repository we have tracks about twelve thousand philosophical figures. Not all those are notable figures, not all have notable works. Each philosophical figure is associated with a one or more texts – by corollary each text is associated with one or more figures. Philosophical figures whose work only appears as fragments in the works of others is a hurdle yet to be surmounted. A text is an abstract object, it represents a single work which is instantiated in editions and these editions are tracked using files. Experiments work on a snapshot of the set of files, this ensures reproducibility. Each file, text, and figure is associated with a set of metadata which are stored as attributes of entities in a relational data-store. As relational databases tend not to excel at full-text search a widely used piece of software called Elastisearch is used for this purpose. A feature of Elastisearch is that it can manage collections of texts as versioned snapshots.

There is a famous quote is software development: “Show me your flowcharts and conceal your tables, and I shall continue to be mystified. Show me your tables, and I won’t usually need your flowcharts; they’ll be obvious.” [Fred Brooks](https://en.wikiquote.org/wiki/Fred_Brooks)

[diagram of relational data]

By text is meant only the content of the text, the paratext is stripped out: this includes all the front matter including publishing and copyright notices, the table of contents, and so on; and the back matter including index. Notes (be they footnotes or endnotes) are a grey area – they were considered not important enough by the author to include in the running text but they clearly impinge on the text itself. (Witness the deluge of instrumental playful endnotes in Infinite Jest).

Due to the way Ruby on Rails dictates the programmer works (what is termed *convention over configuration*, and *don't repeat yourself* (DRY)) each entity has its own screen. There is a set of related screens for working with and manipulating files, a set of related screens for working with and manipulating texts, a set of related screens for working with authors, a set of related screens for working with and exploring on the canon (as this project sees it), a set of screens dealing with search.

One of the consequences of developing this proof of concept was that it became apparent slowly over time time what the semantic web represents for knowledge production. For instance, if Aristotle is an entity in the database then we will want want that entity to be correlated with the Aristotle in the Wikidata data-store, DBpedia data-store, and so on. For notable figures this more or less trivial. For other figures who are less well known in Western culture, or who are harder to pin down owing to the differences in orthographic translations, or by the simple fact that they may share a name with another figure, this process of identification is less automatic and more problematic. This was solved by developing translation tables of names. Also, we tend to forget the entire planet does not speak English – there is something to be said for the time when Latin served as a universal medium of knowledge transmission. Keeping with the example of Aristotle, some of the Stagyrite's main texts have multiple titular renderings. These factors compound to construct very real impediments to faithful knowledge production. An error made in the development of this system is that variants in spelling are hard-coded in code rather than captured in the database schema.

I hesitate to suggest that there is a universal principle at play here: the devil is in the details.

[diagram of system architecture]

resources:

http_request_logger

dictionary

labeling
name

shadow < philosopher
shadow < work
expression(s)

unit

work

text(s)
tag(s)

filter(s) :: work in tandem to save filters
including(s)

author(s)
writing(s)

role
capacity

link – no data!

paths: welcome, philosoraptor, landing, inquiry, collect, springy, semantic_web, dracula, root, do_pome, pome, do_paper, paper, search

root → welcome
welcome → {env, schema, routes}
landing → philosoraptor
inquiry

collect?

search

what needs to be done in version 2.0

Shadow was meant to shadow Wikidata entities. I'd do that properly. I wouldn't have both an author and a philosopher table; by the same token I wouldn't have a text and a work table; hence I wouldn't have a writings and a expressions table. I'd generalise tags/filters/includings.

representativeness and feasibility

Anyway, imagine three sets of philosophical texts: U, P, and C. The first set is the universal set (U), the sum total of all philosophical texts past and present ever housed in a scholarly library and filed under ‘philosophy’. The next set is much smaller, it is a particular (P) collection at a moderately sized academic institution ignoring, given the day and age, texts electronically accessible by the institution. Finally there is the canon (C), the union of sets of texts (with some fuzzy borders) common to most institutions. For instance, it is highly unlikely (to the point of virtual statistical impossibility) that The Republic by Plato would be absent from any decently sized scholarly collection of philosophical texts.

Fun quiz. What are the rough sizes of U, P, and C?

We are confronted with the notion of representativeness[@leechCorpora2002]. Ideally we would like to create a corpus as close in spirit to C as possible. (Yes, I am completely ignoring that notions of the canon are now an ideological battleground.) In order to create this corpus we would need to generate a decently sized sampling of philosophers. For instance all the philosophers indexed by respected comprehensive reference works (dictionaries and encyclopedias) both in print and online. From there choose the notable works for some criterion of notability and voilà a representative corpus.

One key observation here. The list of texts obtained from a rank-ordered list of philosophers and the list obtained by rank-ordering philosophical works irrespective of the author are going to differ. By this I mean that a lesser philosophical figure could produce a highly significant work and likewise a key philosophical figure may have many notable works but no work of the first rank. Undoubtedly the two lists will be mostly in alignment.

I should note here that when I began this process of assembling a directory of philosophers (let us call that, step a), and then piecing together an attendant corpus composed of their notable works (step b), and after that turning them into plain text if the are not already possibly cleaning them up as one goes along (step c).

Onto the digital enquiries, the first of which is turned out to be rank-ordering the list of works.

    • Enquiry #1:
Rank-ordering the list (call this step d for the moment) .

Prior to doing the hard graft this researcher thought that a would be relatively easy, b would be simple once a was done, c would be trivial once b was done, and d would be next to impossible (how on Earth would one come up with an algorithmic measure of relative significance). Spoiler alert, a was pretty easy bar cross-checking between sources but it involved a liberal interpretation of the fair use copyright doctrine, b turns out to be incomplete because I could not figure out how to automate the task of coming up with a list of notable works and tracking them down. (When we are talking about thousands of texts, every step must be automated in order to minimize wasted time). c likewise was tricky and time consuming for reasons that will be explained later, d took a matter of days and was surprisingly successful.

script
issues
a1
./lib/tasks/shadow.rake

bin/rake shadow:philosopher:populate
not all things that are philosophers are humans
a2
./lib/tasks/shadow.rake

bin/rake shadow:philosopher:dbpedia
?
a3
(i..vi)
./lib/tasks/snarf.rake

bin/rake snarf:foo
screen scraped
a4
(i)

./lib/tasks/snarf.rake

bin/rake snarf:CUP
from OCR'd doc
a5
./import/sauce.rb
./lib/knowledge/*/*

bin/rails runner import/sauce.rb
some without dates, differing dates, differing formats, complex formats, variant spellings

hmm

script
issues
d1
./lib/tasks/shadow.rake

bin/rake shadow:philosopher:?
fix up data: mentions, PageRanks, metrical, ordering

step a

So the exact procedure for populating the directory of philosophical figures goes like this.

1. Populate initially with Wikidata.

   This can be done more or less directly with no staging. Due to the intricacies of the Wikibase/DataModel[^model_quote][@wikimediafoundationWikibaseDataModel2014] and the idiosyncrasies of the actual data this was a trial and error process resulting in a moderately robust combination of Ruby and SPARQL. Currently what is retrieved is: unique entity id., name in all languages, date of birth and if deceased a date of death5,

    [^model_quote]: “The data model of Wikibase describes the structure of the data that is handled in Wikibase. In particular, it specifies which kind of information users can contribute to the system. On a more abstract level, the Wikibase data model provides a metamodel or ontology for describing real world entities. Such descriptions are concrete models for real world entities.”

2. Then with DBpedia.

   This is where the invaluable PageRank data comes from.

3. Screen scrape data from websites:

   These are staged in files for later importing.

   i. Macmillan Reference USA, The Encyclopedia of Philosophy
   ii. Internet Encyclopedia of Philosophy, The Internet Encyclopedia of Philosophy (IEP)
   iii. Garth Kemerling Philosophy Pages, A Dictionary of Philosophical Terms and Names
   iv. Oxford Reference, Oxford Dictionary of Philosophy
   v. Routledge, Taylor & Francis Group, Routledge Encyclopedia of Philosophy
   vi. Metaphysics Research Lab, CSLI, Stanford Encyclopedia of Philosophy

4. Read data from OCR'd file:

   The only one not online. Found a decent PDF, used the trust Tesseract open-source OCR software.

   i. Cambridge University Press, The Cambridge Dictionary of Philosophy

5. Import data

   My very own rectification of names. See appendix B.

6. Fix up data and rank-order them.

This is another ordered list.

1. Populate initially with Wikidata

    Use SPARQL-fu

    Grab everyone who is instance-of philosopher + instance-of human (to eliminate a very very small number of quirky entries that are both organisations and ‘philosophers’) – while we're at it grab date of birth and date of death and language (if English use that, otherwise use the first given …) should really grab all – this is not a defect as such, it could be described as a shortcoming.

    12,358 entries

2. Then with DBpedia

    Again with the SPARQL-fu. Cross-check, names that don't match check to see if it because of a variant spelling or if they are totally new. Record any variant spellings.

    1,752 entries

3. Screen scrape data from reference works

    Write a custom screen scraper for each site. I emailed each of them asking for permission but I never heard back from any of them.

    There was another site: Runes from 1942 uploaded by Андрій Хруцький (Andrew Chrucky) but there were startlingly few entries concerning women so I took it to be a product of its times and excluded the data from the directory as too biased.

    Macmillan: 1,028
    Routledge:   991
    Oxford:      690
    Cambridge:   589
    Stanford:    414
    Kemerling:   408
    IEP:         280

    I ended up with many variant spellings. Some variants due to editorial decisions on how to spell foreign names.

    Total:    12,705

    There are doubtless many minor philosophical figures not in this list. The list is skewed towards established figures. As I am going for representativeness and comprehensiveness this is not a problem? If there any glaring omissions or entries that shouldn't be in there (specially from Wikidata source) then there could be a way to use voting by consensus to remove them. As I'm going to give them all a weighting this may not be necessary because the least representative will surely get low weightings.

step b

The texts were collected independently initially.

* Philosophy (Bookshelf) - Gutenberg
* Philosophy Collection - eBooks@Adelaide
* Philosophy - Online Library of Liberty
* Marxists Internet Archive Library – MIA
* Full list of texts – The Anarchist Library
* The Internet Classics Archive | Browse - MIT
* Online Full Philosophical Texts - Philosophy Index
* Philosophy Collection - EServer
* Online Papers and Books - DiText
* Past Masters: Full Text, All Philosophy - Intelex
* [philosophy] - Hathi Trust Digital Library
* Philosophy by Title - Christian Classics Ethereal Library
* /texts/ - Hanover College, History Department
* Perseus Digital Library - Tufts University
* Digital Collections - U Michigan Library
* Early Modern Philosophy - Early Modern Texts
* [philosophy] - Internet Archive
* Category of Philosophy - Wikisource, the free online library

* Important publications in philosophy - Wikipedia
* Free Philosophy eBooks | Open Culture

The system distinguishes texts from files. A text is ultimately associated with one file. A text is also associated with a number of authors. Each of these is represented by a table in a relational database. Each row in a table holds data on an individual file, text, or author. Each table has a number of columns, each representing a different attribute of the entity in question.

A file is specified using a URL, other attributes include content type, size, health, and a name which usually is a combination of the author and part of the title of the text. The last attribute is not strictly necessary but it does make it easier to identify files especially if the URL is cryptic.

Section titles in a text are important for understanding the layout of a text but they are not needed for computational linguistic purposes. At the moment the section names are recorded which would mess with the algorithms. Hence the necessity of stop-words. Ultimately an compromise solution may be found such as using ALL CAPS for section names and having the algorithms ignore them.

list_of_philosophical_works.ods

step c
The system stores every file as Unicode (UTF-8 to be precise, this is the default in modern Linux systems), in order to do so it must transcode from whatever character encoding is advertised in the HTTP header, this is an implementation detail and I include it here to give an idea of the non-trivial nature of getting the plain text of a file. The system recognises four different file types: plain text, HTML, TEI (XML), and PDF. In the first instance no additional processing is necessary. In the second and third instances a built-in Ruby method was used to turn marked up text into plain text. In the final instance an external program was used.

It was much later that a health measure was included. This feature is a work in progress–the closer to 00.00% the measure the better the health. Adding this allowed me to see that a number of texts were mangled when translating from PDF to text. Because of this I had to use a professional OCR program called Tesseract. These files were stored on my own website: [Philosophical Texts](http://leto.electropoiesis.org/propaganda/philosophical-texts/).

step d

The surprising part.

I wanted to be able to rank figures by some measure of ‘significance’ whatever that means. This would appear to be one of those squidgy hard-to-quantify humanities things that fit right into the subjective/qualitative research basket. In the end the metric I chose was this–normalise each entry between zero and one, assign a weighting to each source chosen semi-intuitively, using a custom DBpedia query based on the research of [Andreas Thalhammer](http://people.aifb.kit.edu/ath/) get a ranking for every figure even ones not designated as philosophers by DBpedia, count how many times the words philosopher and philosophy (translated into all 386 languages – Name.pluck(:lang).uniq.count) are used, count the number of various Wikipedia Foundation sites the figure has. combine them all to get a metric.

A surprising result!
15 out of 20 compared to Leiter Reports Survey [@leiter20MostImportant2009] with the next two in 25th and 26th positions, and the only outlier being Frege in 79th . (this is for all time)
“
With nearly 900 votes cast, we now know:

1. Plato  (Condorcet winner: wins contests with all other choices)
2. Aristotle  loses to Plato by 367–364
3. Kant  loses to Plato by 411–328, loses to Aristotle by 454–295
4. Hume  loses to Plato by 534–166, loses to Kant by 533–176
5. Descartes  loses to Plato by 597–117, loses to Hume by 356–269
6. Socrates  loses to Plato by 548–101, loses to Descartes by 327–270
7. Wittgenstein  loses to Plato by 610–85, loses to Socrates by 385–193
8. Locke  loses to Plato by 659–29, loses to Wittgenstein by 311–239
9. Frege  loses to Plato by 611–86, loses to Locke by 279–256
10. Aquinas  loses to Plato by 642–57, loses to Frege by 289–284
11. Hegel  loses to Plato by 615–82, loses to Aquinas by 288–285
12. Leibniz  loses to Plato by 650–36, loses to Hegel by 281–266
13. Spinoza  loses to Plato by 653–49, loses to Leibniz by 281–207
14. Mill  loses to Plato by 645–39, loses to Spinoza by 272–247
15. Hobbes  loses to Plato by 647–47, loses to Spinoza by 269–245
16. Augustine  loses to Plato by 663–46, loses to Mill by 296–247
17. Marx  loses to Plato by 653–52, loses to Augustine by 305–248
18. Nietzsche  loses to Plato by 691–63, loses to Marx by 327–269
19. Kierkegaard  loses to Plato by 622–106, loses to Nietzsche by 330–256
20. Rousseau  loses to Plato by 638–41, loses to Kierkegaard by 280–209

Berkeley was a close runner-up for the top 20.

The top six are not surprising (though they wouldn't have been my top six, but that's another matter), but after that the results reveal how radically people's conceptions of philosophy diverge.  Wittgenstein ahead of Locke, Hegel, Spinoza, Mill et al.?  Augustine ahead of Marx and Nietzsche?  Aquinas in the top ten?  What explains it?  Thoughts from readers?
”
(Reproduced with permission)

From the Leiter Reports correspondents perspective:

    1.  Plato           2
    2.  Aristotle       1
    3.  Kant            6
    4.  Hume           18
    5.  Descartes       5
    6.  Socrates        4
    7.  Wittgenstein   20
    8.  Locke          12
    9.  Frege          79
    10. Aquinas        14
    11. Hegel          15
    12. Leibniz        11
    13. Spinoza        31
    14. Mill           25
    15. Hobbes         26
    16. Augustine      16
    17. Marx            3
    18. Nietzsche       7
    19. Kierkegaard    34
    20. Rousseau       13

Fifteen also in the top 20! Four in the top 40, one outlier.
* Wittgenstein: “Wittgenstein ahead of Locke, Hegel, Spinoza, Mill et al.?” (Not ahead of Locke, but yes ahead of Spinoza and Mill – Leiter demonstrate personal bias?)
* Frege: The outlier, not a general philosopher, obviously very important in logic (functional algebra) but not as “popular” or “canonical” as survey respondents think.
* Aquinas: “Aquinas in the top ten?” Not in the top ten in the directory, but still placed very highly – consider Augustine, if Augustine rates why not the premier Church scholar?
* Spinoza, Mill, Hobbes, Kierkegaard: At variance with respondents … why?
* Augustine: “Augustine ahead of Marx and Nietzsche?” Not ahead of Marx and Nietzsche in the directory.

From the directory's perspective – from all time:

position   name                           d.o.b        rank
--------   ----------------------------   -------      ----
     1.    △Aristotle                     b. -383        2
     2.    ▽Plato                         b. -426        1
     3.    △Karl Marx                     b. 1818       17
     4.    △Socrates                      b. -469        6
     5.    –René Descartes                b. 1596        5
     6.    ▽Immanuel Kant                 b. 1724        3
     7.    △Friedrich Nietzsche           b. 1844       18
     8.    –Cicero                        b. -106        –
     9.    –Voltaire                      b. 1694        –
    10.    –Bertrand Russell              b. 1872        –
    11.    △Gottfried W. Leibniz          b. 1646       12
    12.    ▽John Locke                    b. 1632        8
    13.    △Jean-Jacques Rousseau         b. 1712       20
    14.    ▽Thomas Aquinas                b. 1225       10
    15.    ▽Georg W. F. Hegel             b. 1770       11
    16.    –Augustine of Hippo            b. 354        16
    17.    –Adam Smith                    b. 1723        –
    18.    ▽David Hume                    b. 1711        4
    19.    –Michel Foucault               b. 1926        –
    20.    ▽Ludwig Wittgenstein           b. 1889        7

Interesting extras here at variance from the survey.
* Cicero: Key stoic philosopher,
* Voltaire: Author of a satiric dictionary of philosophy, key figure of the Enlightenment – taught tangentially but not directly
* Russell: Leads a triple life, History of Western Philosophy
* Smith: if Marx is important, so is Smith
* Foucault: giant of Continental philosophy

With Wikidata pagerank numbers

position   name                           d.o.b        rank
--------   ----------------------------   -------      ----
     1.    △Aristotle                     b. -383        2
     2.    ▽Plato                         b. -426        1
     3.    △Karl Marx                     b. 1818       17
     4.    △Socrates                      b. -469        6
     5.     René Descartes                b. 1596        5
     6.    ▽Immanuel Kant                 b. 1724        3
     7.    △Gottfried W. Leibniz          b. 1646       12
     8.    △Jean-Jacques Rousseau         b. 1712       20
     9.    △Friedrich Nietzsche           b. 1844       18
    10.     Bertrand Russell              b. 1872        –
    11.     Pythagoras                    b. -572        –
    12.     Cicero                        b. -106        –
    13.     Voltaire                      b. 1694        –
    14.    ▽John Locke                    b. 1632        8
    15.    ▽Thomas Aquinas                b. 1225       10
    16.    ▽Georg W. F. Hegel             b. 1770       11
    17.    ▽Ludwig Wittgenstein           b. 1889        7
    18.     Jean-Paul Sartre              b. 1905        –
    19.     Pascal                        b. 1623        –
    20.    ▽David Hume                    b. 1711        4
    21. 
    22.     Confucius                     b. -550        –
    23.     Augustine of Hippo            b.  354       16
    24.     Adam Smith                    b. 1723        –
    25.     Michel Foucault               b. 1926        –

\newpage

position   name                           d.o.b        rank
--------   ----------------------------   -------      ----
     1.     Ayn Rand                      b. 1905       91
     2.     Hannah Arendt                 b. 1906      104
     3.     Mary Wollstonecraft           b. 1759      120
     4.     Simone de Beauvoir            b. 1908      123
     5.    ▷Martha Nussbaum               b. 1947      205
     6.     G. E. M. Anscombe             b. 1919      220
     7.    ▷Julia Kristeva                b. 1941      234
     8.     Hypatia                       b.  370      251
     9.     Simone Weil                   b. 1909      258
    10.     Rosa Luxemburg                b. 1871      320
    11.     Anne Conway                   b. 1631      383
    12.     Edith Stein                   b. 1891      389
    13.     Hildegard of Bingen           b. 1098      393
    14.    ▷Luce Irigaray                 b. 1930      404
    15.     Iris Murdoch                  b. 1919      407
    16.     Philippa Foot                 b. 1920      449
    17.    ▷Judith Butler                 b. 1956      468
    18.     Elisabeth of the Palatinate   b. 1618      500
    19.    ▷Hélène Cixous                 b. 1937      571
    20.    Susanne Langer                 b. 1895      584

Table: From all time – female

Five of the all-time female philosophical figures are still living: Martha Nussbaum, Julia Kristeva, Luce Irigaray, Judith Butler, and Hélène Cixous – I've marked them with a “▷” in the list.

\newpage

position   name                           d.o.b        rank
--------   ----------------------------   -------      ----
     1.    Noam Chomsky                   b. 1928       30
     2.    Jürgen Habermas                b. 1929       40
     3.    Daniel Dennett                 b. 1942       80
     4.    John Searle                    b. 1932      102
     5.    Saul Kripke                    b. 1940      152
     6.    Thomas Nagel                   b. 1937      189
     7.    Jerry Fodor                    b. 1935      196
     8.    Alasdair MacIntyre             b. 1929      208
     9.    Charles Taylor                 b. 1931      210
    10.    Peter Singer                   b. 1946      266
    11.    Stanley Cavell                 b. 1926      363
    12.    Alvin Plantinga                b. 1932      379
    13.    Hossein Nasr                   b. 1933      396
    14.    Slavoj Žižek                   b. 1949      408
    15.    Nicholas Rescher               b. 1928      433
    16.    Michael Sandel                 b. 1953      474
    17.    Cornel West                    b. 1953      514
    18.    Jean-Luc Nancy                 b. 1940      534
    19.    Richard Swinburne              b. 1934      538
    20.    Giorgio Agamben                b. 1942      556

Table: Living male – as of early 2019

Youngest are Michael Sandel and Cornel West, both sprightly in their late 60s.

\newpage

position   name                           d.o.b        rank
--------   ----------------------------   -------      ----
     1.    Julia Kristeva                 b. 1941       205
     2.    Martha Nussbaum                b. 1947       234
     3.    Luce Irigaray                  b. 1930       404
     4.    Judith Butler                  b. 1956       468
     5.    Hélène Cixous                  b. 1937       571
     6.    Judith Jarvis Thomson          b. 1929       615
     7.    Patricia Churchland            b. 1943       626
     8.    Sandra Harding                 b. 1935       829
     9.    Susan Haack                    b. 1945       844
    10.    Nancy Cartwright               b. 1944       888
    11.    Ruth Millikan                  b. 1933       918
    12.    Mary Midgley                   b. 1919       983
    13.    Carol Gilligan                 b. 1936      1005
    14.    Gayatri Chakravorty Spivak     b. 1942      1034
    15.    Michèle Le Dœuff               b. 1948      1053
    16.    Angela Davis                   b. 1944      1057
    17.    Rebecca Goldstein              b. 1950      1086
    18.    Onora O'Neill                  b. 1941      1142
    19.    Christine Korsgaard            b. 1952      1165
    20.    bell hooks                     b. 1952      1228

Table: Living female

Practically the same age range as their male counterparts, ranked a good deal lower.
Future directions: social networks (who knew who and who influenced who) and subject areas. Then one can ask questions like: “Who influenced X”? and “Who did Y influence” and “Show me the main thinkers from 1850 until the present in the area logic in chronological order” and “Show me who wrote a good deal about political philosophy from ancient times until the present in order of significance”

step b revisited

* Because rank-ordered appears to work so well, rather than attempting to get a couple of work for all ~12,500 philosophers, choose the first ~1,250 and select one work or many for each.
* Figure out some kind of automated way of getting the notable works for each philosopher

interface to directory

Figure 1: Philosopher Screen UX

First iteration: Rails + Elastisearch

Second iteration: Rails + VUE.js + Elastisearch

proof of concept / prototype.

collaborative proof of concept

    • Enquiry #2a
Domain modelling (Galway):[@bordeaSaffronInsightCentre2014]

Saffron[@bordeaDomainAdaptiveExtraction2013] is a domain modelling research tool that uses novel natural language parsing and taxonomic techniques. Specifically, it performs domain adaptive extraction of topical hierarchies. Saffron was conceived and is being developed at the Insight Centre for Data Analytics in the National University of Ireland, Galway.
We arrive at a taxonomy in a number of algorithmic steps.
(1) By performing a semantic-based analysis of the PACT.x corpus using Saffron we obtain a result set α, Rα, which comprises a sequence of terms, Sα (Table 1), and related taxonomy, Tα, of philosophical concepts (Vulcu 2015) and their corresponding visualisation (Figure 2). Sequence Sα is ordered by a weighted combination of frequency and coherence, meaning frequency of occurrence of a word within the corpus coupled with terms that contain a word from the domain model, and terms that appear in the context of a word from the domain model. The structure and relation of the topics in taxonomy Tα are drawn from result set Rα. The directed edges of the graph are constructed using the broader/narrower than relation from WordNet and the graph is pruned using the ChuLiu/Edmonds algorithm resulting in a directed acyclic graph.

-------------    --------------
  1 Nature       16 Truth
  2 Life         17 Matter
  3 God          18 Cause
  4 Knowledge    19 Mind
  5 Law          20 Term(s)
  6 Word(s)      21 Object
  7 Principle    22 Character
  8 Sense        23 Soul
  9 Power        24 Person
 10 World        25 Philosophy
 11 Time         26 Moral
 12 Reason       27 Pleasure
 13 Body         28 Experience
 14 Idea         29 Government
 15 Form         30 Action
---------------------------------

Table 1: First 30 terms (in sequence A) in Insight domain model

An analysis of the above!!

Figure 2: Saffron Screen UX

And an analysis of the above!!

from vocabularies to taxonomies to ontologies

A key figure for me as I have said before is J.F. Sowa, the work I am drawing on is the book Knowledge Representation[@sowaKnowledgeRepresentationLogical2000], personal correspondence: for example[@keelerLongDiscussionJohn2017] and [@sowaProposedStandardOntology2016], Peirce-L and Ontology-Forum mailing list posts, the article “Signs and Reality”[@sowaSignsReality2015].
* Sowa Ontology (based on the semeiotics of Peirce)

  Another central figure is Smith, see "Ontology"[@smithOntology2004] in the Floridi edited Philosophy of Computing and Information, his later collab with Grenon, "Foundations of an Ontology of Philosophy"[@grenonFoundationsOntologyPhilosophy2011].

* Grenon/Smith Ontology (based on Basic Formal Ontology derived from Husserl's Logical Investigations)

  Pavel Garbacz, a Polish logician, see "Challenges for Ontological Engineering in the Humanities – A Case Study of Philosophy"[@garbaczChallengesOntologicalEngineering2015].

* OntOfOnt Ontology (based on an analysis of and claims to supersede)
  + so-called LoLaLi Ontology
  + so-called Korean Philosophy Ontology
  + PhiloSurfical Ontology
  + Indiana Philosophy Ontology
  + PhiloSpace Ontology

controlled vocabulary

taxonomy
Subsumptive containment hierarchy
A subsumptive containment hierarchy is a classification of object classes from the general to the specific. Other names for this type of hierarchy are "taxonomic hierarchy" and "IS-A hierarchy".[8][13][14] The last term describes the relationship between each level—a lower-level object "is a" member of the higher class. The taxonomical structure outlined above is a subsumptive containment hierarchy. Using again the example of Linnaean taxonomy, it can be seen that an object that is part of the level Mammalia "is a" member of the level Animalia; more specifically, a human "is a" primate, a primate "is a" mammal, and so on. A subsumptive hierarchy can also be defined abstractly as a hierarchy of "concepts".[14] For example, with the Linnaean hierarchy outlined above, an entity name like Animalia is a way to group all the species that fit the conceptualization of an animal.

Various stabs at an ontology for philosophy or with philosophical applications:

* [J.F. Sowa – Peircean Ontology](http://www.jfsowa.com/ontology/)
* [W3C – Semantic Web Ontology](https://www.w3.org/standards/semanticweb/ontology)
* [Pawel Garbacz – MetaOntology](http://metaontology.pl/)
* [Smith et al – Basic Formal Ontology](http://basic-formal-ontology.org/)
* [Wikipedia list of Upper Ontologies](https://en.wikipedia.org/wiki/Upper_ontology)

> Peirce's theory of types and relations subsumes every version, formal or informal, that anyone has ever proposed.
Peirce does not have a theory of types. He was getting there but I don't think he saw it. Heresy, I know :) Proper type theory could (and did) only happen as a response to the development of actual real-world computing automata. We can see echoes of type theoretical thinking after the fact (in lots of thinkers) but it would be a-historical to claim that Peirce saw something he did not see. The first person to kind of see this was Russell and then properly after him Church and after him Martin-Löf. If anyone before Church could have come close to articulating it coherently it would have been Peirce however.
What Peirce arrived at was a logic-as-semiotics. In the mirror it bears a close resemblance, and I think that it is this linkage that scholars of Peirce must demonstrate, have still yet to demonstrate.
For existential graphs to be type theoretic, all the building blocks must be typed: the icons, the identities, the nesting, negation, … One ought to be able to construct the Type Nothing and and name it so. Essentially one needs to be able to say things like Γ ⊢ : ⊥
“In this context there is the judgement that to match nothing is designated Nothing.” This constructs the empty type. How would one construct an empty type using existential graphs? Types are first-class citizens so to speak. If you squint they look like hierarchies of sets, if you squint again they look like logical constants.
> I've known Chris and Hugh for years. They're database designers who know enough logic to do a decent job.
Great! So what I'm saying should make sense to you. What I've been trying to articulate to you and to Mary and to Amy is how all these advances (be it (a) the semantic web with RDF/OWL/SKOS/_whatever_ and (b) typed lambda calculus and (c) Date and Darwen's work) are converging on a very simple pattern, one that Peirce stumbled upon but failed to articulate fully in my humble opinion.
> In fact, every version of type theory is a theory about methods for defining monadic predicates.
Are you sure? Types represent both _form_ and _content_.
> YAFOM
Yes, I never said otherwise. This could be more succinctly put with the following question: how can a foundation serve as its own foundation? Or: foundation has a meaning in the concrete world of architecture and buildings, what happens when we use the word metaphorically to speak about abstractions?
I'll go further than you though. I don't _care_ about mathematics. I only care about how type theories can usefully talk about the types of things that philosophers care about. In so far that philosophers talk about mathematics I care about mathematics. I used to think I cared about math, I realise now that it is abstraction I am drawn to, math just happens to inhabit part of that space.
What I'm going to talk about at the workshop is: "that's a great type theory you have there, why is it that type for you only means type of mathematical entity?"
Quoting myself

future enquiries

    • Enquiry #2b: Domain modelling revisited with Insight Centre, Galway
      This time with a larger dataset, with cleaned up texts, the K.J.V. bible is not in the dataset

Where does Saffron stand today? Cite documentation and published papers. John McCrae and others

“Saffron is a tool for providing multi-stage analysis of text corpora by means of state-of-the-art natural language processing technology. Saffron consists of a set of self-contained and independent modules that individually provide distinct analysis of text. These modules are as follows:

    • *Corpus Indexing*: Analyses raw text documents in various formats and indexes them for later components
    • *Topic Extraction*: Extracts keyphrase that are the topics of each single document in a collection
    • *Author Consolidation*: Detects and removes name variations from the list of authors of each document
    • *DBpedia Lookup*: Links topics extracted from a document to URLs on the Semantic Web
    • *Document-Topic Analysis*: Analyses the topics of a document and finds the relative importance of these topics
    • *Author-Topic Analysis*: Associates authors with particular documents and identifies the importance of the document to each author
    • *Topic Similarity*: Measures the relevance of each topic to each other topic
    • *Author Similarity*: Measures the relevance of each author to each other author
    • *Taxonomy Extraction*: Organizes the topics into a single hierarchical graph that allows for easy browsing of the corpus and deep insights.

One can either use the defaults as provided or go into the advanced options and adjust the parameters of the run/execution.

One can set the maximum number of topics extracted, the default is 100. Note that topic here refers to an extracted term, unlike with topic modelling where topic is a misnomer used to refer to a bag of terms statistically determined.

Minimum tern frequency signifies the number of occurrences across the corpus.

Terms are scored according to features in the text. These features are combined using mean reciprocal ranking. As with the _ ranking of philosophical figures and works these scores are scaled to between 0 and 1. There is a threshold below which should topics fall they won't be considered. The default is 0 which means that no topic is left behind.

features:
* Total TF-IDF
* Residual IDF
* ComboBasic ?
* cValue
* Weirdness
* +

Blacklisting and whitelisting

Ref Corpus → weirdness/relevance

One can adjust the Top N max pairs of Author-Topic

One can adjust the number of related authors each author can have for Author-Similarity

Topic-Similarity is determined by the number of documents are shared by a pair of topics

Term extraction is a NLP problem first and foremost with domain knowledge layered on top.

Taxonomy extraction relies on textual features with the actual pruning/ordering of the set being an NP complete problem. Certain strategies are used to make it tractable.

    1. Greedy
    2. Beam (fixed stack)
    3. Minimum spanning p (Georgeta's pruning algo.)

? pair-wise: Sarkar word embediness / sub-sequences

These three use different scoring methods such as simple and transitive, BhattacharryaPoisson,

Difference between stopwords and blacklisting

    • Enquiry #3a: Topic modelling with Derek Bridges, UCC
      Will be able to compare an augmented LDA topic modelling approach to the Saffron domain modelling approach.
    • Enquiry #3b: Term extraction and correlation analysis
      …
    • Enquiry #3c: Topic modelling, in-house
      Method using hardware accelerated machine learning
      Use which tool? Thomas's? MALLET? TMT? Gensim? Which?
    • Enquiry #4: Word vectoring, in-house
      Which tool? SpaCy, Google, Facebook, IBM, …
    • Enquiry #5: Divisions deduced dynamically
      
    • Enquiry #6: Area/Branch and Geographic analysis with Mark Alfano,
      Data taken from Wikidata, longitudinal analysis
      Ability to make queries like: “display European logicians 1800-present
    • Enquiry #7: Semantic mapping with Mark Alfano, 
      Similar to his work on Nietzsche corpus with DR2 Open Peer Review
    • Enquiry #8: ? with Ferando Nasc (and others) of the Digital Ricouer Project
      …
    • Enquiry #9: EAQL + EATS (Electronic Archive (Query Language + Type System))

## Not Against Method

Quick recap. What ultimately prompts this work? One thing alone. Digital transformation. And what is that fundamentally? A change in medium – from old to new. This brief though profound inflection moment in human history, this moment is intramedial. Recall our analysis: content, form, method, medium. As it is the change in medium that is primary – changes in content, form, and method are knock-on causes. Herein, our focus is narrowed in the sense that we grapple not with what digital transformation means in toto6 – we consider the inflection only from the perspective of philosophy. The previous chapter dealt with what must happen to the content of philosophy in light of the digital transformation; the next chapter deals with what must happen to the form philosophy takes in the same light; this chapter considers what happens when philosophical methods go digital.

First the obvious question, what precisely do we talk about when we talk about method? Method here refers to the efficient cause of a thing (anything) and so can refer one the one hand to the entity that brought the thing into being and on the other it can refer to the process. If the thing in question is an artefact then one could talk about the craftsperson or the craft, if the thing in question is a concept then one could talk about the thinker or the thought processes. Most astonishingly following this line of reasoning to its logical conclusion we end up in the territory of robot philosophers and a robotic philosophy. 

Retreating from science fiction to the present day let us consider here what it means for philosophical methods to be augmented with computational methods. As I argued in the previous chapter philosophy is not methodologically special, neither is its literature formally special. (How could it be?) Philosophers neither have a monopoly on any aspect of the way in which philosophy is done and nor do they have a monopoly on the modes of representation and reckoning. Not only does no one person have a their own private language as Wittgenstein pointed out no group of people do. This implies that what the digital humanities (and digital sciences) share is a methodological commons and a formal commons. We explore the formal commons in the next chapter.

To reiterate: when a philosopher takes a concept like justice and investigates it philosophically they do so using mental machinery that is not the sole privilege of philosophers. To reduce philosophy to conceptual analysis while methodologically sound runs the risk of us implying (however ridiculous that may be) that no other rational entity reasons thus – that is to say, surely we cannot mean that no other disciplined thinker or scholar analyses concepts? Similarly when a philosopher communicates their ideas they do so using literary forms (let us confine ourselves to the written word for argument's sake) whether major or minor or forms of expression that are not the exclusive purview of that in-group.

## Excursus

A brief digression, via Ryle, to matters metaphilosophical,

But more important than these nuisances, preoccupations with methods tends to distract us from prosecuting the methods themselves. We run, as a rule, worse, not better, if we think a lot about our feet. So let us, at least on alternate days, speak instead of investigating the concept of causation. Or, better still, let us, on those days, not speak of it at all but just do it. [@ryleOrdinaryLanguage1953]

<!-- how do i do the following? (Ryle 1953, p. 185) -->

A variant on this recently enough was dropped into my Inbox one who should know better – by which I mean a practitioner of computational philosophy and thus a sorcerer of the new realm – and at other times by others in both informal and scholarly face-to-face settings that they have little time for metaphilosophical investigations. This is not at all an uncommon position within the community of philosophers. Many are those that like to put on a workmanlike show of saying that we ought to just roll up our sleeves and get on with the business of doing philosophy and not distract ourselves with mere metaphilosophical concerns because after all isn't philosophy inherently metaphilosophical by nature?

Is not the implication that one ought to down methodological tools, pick a more appropriate niche, and get on with better things?

Digital humanists rarely if ever fall for this line of reasoning. The reason being that digital humanists have already been converted to the cause. From this perspective (the perspective of proselyting and conversion) one can say that the project of the digital humanities will have achieved completion when the vast majority of scholars are converted. As the famous Athenian syllogism starts off: “all men are mortal” and though it is morbid to point it out, as Kuhn has observed often-times for a more robust theory or paradigm to supplant a less robust one all that needs to happen to the old guard who are wedded to the old ways is to let time do what time does. In the interim what would be the most robust response to why bother? The answer we can give here applies to any discipline: it is precisely because we are working our way through this intramedial period, this moment of digital transformation, that it is demanded of us that we re-evaluate our subject matters and our methods of scholarship and our forms of expression. And if not us, who? We, digital humanists, see with varying degrees of clarity that this ultimately is the project we are involved in.

More specifically. Why metaphilosophy? Again, because digital transformation. It demands this of us. One cannot, in fact, not. Besides, we philosophers know that philosophy is not just any old discipline. There, I said it. We know that there is something special about philosophy. The first long-form work of philosophy I undertook under my own steam was a work that attempted to answer the question why is it that metaphysics changes very slowly if at all. The reason for answering that question robustly is that this aspect of philosophy (the unchangingness of metaphysics) is used by those who value little the works of philosophers to call the entire enterprise into question. There is no need to give specific examples of this specious argument here. Anybody who has hacked at the thickets surrounding philosophy will have encountered this argument at least once. Those who alight on this quirky aspect of the discipline should hesitate before they rejoice that they have uncovered a millennia-old bug in philosophy. As they say in the realm of software development – that ain't a bug, it's a feature. In my juvenile work I argued that because natural language mutates slowly over time each generation must dress philosophy in the garb of its own time. But that does not tackle why philosophy genuinely appears to change very little over time. Nobody can argue that it does not change at all…
expression/content → discipline which structures disciplines

And now back to our original scheduled programming.)

(be)causes

content →

← content

metaphilosophy
“on coming to terms”

expression →
hybrid writing forms and lexical automata
“from – to – ”

← expression

data philosophy & philosophic computing
“towards – ”

method →

← method

medium →
digital
“into the age of inflection”
oral / written / print
← medium

typed semiotics / semiotic types
“by reverting to type”

semiotic differential

content
expression
method
medium
scope

concreteness

mode
representation: content words (topic modelling)
representation: function words (computational stylometry)
reasoning: logical form (computational metaphysics)
what does it mean to reason about method computationally?

power

axes

rectification of names
    • standardisation
    • correct mapping

rectification of dates
    • standardisation
    • correct mapping
    • approximate dates
