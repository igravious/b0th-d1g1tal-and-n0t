
OUTPUT='glossary.md'
SOURCE_LIST='source.list'
THE_EXT='.gloss'
THE_DIR='glossary'

puts "Remaking #{OUTPUT}"
# load ERB file, ERB file cycles through each glossary entry
digitature = File.read('template.md.erb')
require 'erb'
erb_template = ERB.new(digitature)

Dir.chdir('..')

SOURCE_LIST_G = File.readlines(SOURCE_LIST) - [OUTPUT+"\n"]
all_glosses = []

def boyzIImen(str)
	File.join(THE_DIR, str.gsub(' ', '_') + THE_EXT)
end

def menIIboyz(fn)
	File.basename(fn, THE_EXT).gsub('_', ' ')
end

SOURCE_LIST_G.each do |fn|
  fn = fn.strip!
  puts "processing #{fn} …"
	src = File.read(fn)
	glosses = src.scan(/__([^_][^_]*)__/)
	all_glosses += glosses
	puts fn + ": " + glosses.inspect
	glosses.each { |gloss|
		str = gloss.first
		fn = boyzIImen(str)
		# File.write(fn, "## #{str}\n\n") unless File.exist?(fn)
		File.write(fn, "…") unless File.exist?(fn)
	}
end

all_glosses.flatten!

@entries = Dir.glob(File.join(THE_DIR, "**#{THE_EXT}")).sort

# a bit inefficient
@entries.reject! {|fn| !all_glosses.include?(menIIboyz(fn)) or File.read(fn).empty?}
require "ostruct"
@entries.map! {|fn| OpenStruct.new(:heading => menIIboyz(fn), :body => File.read(fn))}

File.write(OUTPUT, erb_template.result(binding))
