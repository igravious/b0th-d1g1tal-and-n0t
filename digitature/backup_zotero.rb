begin
  require 'active_support/core_ext/hash/keys'
rescue LoadError
  STDERR.puts 'where is `active_support/core_ext/hash/keys` ?'
end

module Zotero
  module Entities; end
  module Styles; end
end

ff = Gem.find_files('zotero/**/*.rb')

if ff.empty?
  STDERR.puts 'you might want to figure out why `Zotero/**/*.rb` is empty'
else
  ff.each { |path| require path }
end
