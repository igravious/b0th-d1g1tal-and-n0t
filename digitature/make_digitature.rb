require 'rubygems'
require 'bundler/setup'

require 'pry'
require 'rb-readline'

require 'json'

require 'zotero'
ZOTERO_USER_ID = '1248965'
ZOTERO_KEY = File.read('private_zotero_key')
puts '1'
library = Zotero::Library.new ZOTERO_USER_ID, ZOTERO_KEY

# binding.pry

puts '2'
@collections = library.collections

puts '3'
@collection = @collections.select{|x|x.name == 'emerging philosophy'}.first

puts '4'
@entries = @collection.entries

puts "processed #{@entries.length} entries in the collection"

# load ERB file, ERB file cycles through each entry, the review snippet is held in the note portion of Zotero!

TEMPLATE = 'template.md.erb'
puts "attempting to read the ERB template #{TEMPLATE} …"
digitature = File.read(TEMPLATE)

require 'erb'

erb_template = ERB.new(digitature)

MARKDOWN = '../appendix.md'
puts "interpolating the template into Markdown #{MARKDOWN} …"
File.write(MARKDOWN, erb_template.result(binding))
