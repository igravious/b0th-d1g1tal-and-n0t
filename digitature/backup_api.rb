require 'rest-client'
require 'json'

class Zotero::Api
  def initialize(user_id, key)
    @user_id = user_id
		@offset = "https://api.zotero.org/users/#{@user_id}/".length+1
    @key = key
  end

  def get(path_fragment)
		next_fragment = path_fragment
		more_to_do = true
		chunks = []
		while more_to_do
			# p next_fragment
			got = RestClient.get("https://api.zotero.org/users/#{@user_id}/#{next_fragment}", {
				'Zotero-API-Version' => 3,
				'Zotero-API-Key' => @key
			})

			link = got.headers[:link]
			more_to_do = false
			chunks += ::JSON.parse(got.body)

			if "" == link
			else
				splat = link.split(", ")
				if splat.length > 1
					splat.each{|rel|
						if rel.end_with?("; rel=\"next\"")
							next_fragment = rel[@offset..-14]
							more_to_do = true
							next
						end
					}
				end
			end
		end
		chunks
  end
end
