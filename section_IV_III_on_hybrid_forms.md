
# Form – On Hybrid Forms and Lexical Automata

\label{Form – On Hybrid Forms and Lexical Automata}

## Abstracta, Individua, Distincta

Literary criticism is perhaps not equipped to deal with the transition to digital media.

To date, all formal, technical, and typological analyses of electronic literature (and poetry) that I am aware of have approached the domain from pretty obvious directions.

\pagebreak

Examples:

reader-response critique :: [@aarsethCybertextPerspectivesErgodic1997] :: ergodic literature

percepto-cognitive theory :: [@bootzReaderReaders2004] :: aesthetics of frustration

taxonomic :: [@morrisNewMediaPoetics2006] :: (Literal art, Poem-games, Programmable procedural computer-poems, Real-time reiterative programmable poems, Participatory networked and programmable poems, Codework)

historical :: [@funkhouserPrehistoricDigitalPoetry2007] :: archaeology of forms

philosophical :: [@haylesElectronicLiteratureWhat2007] :: (hypertext fiction, network fiction, interactive fiction, locative narratives, installation pieces, "codework," generative art and the Flash poem)

taxonomic :: [@dirosarioElectronicPoetryUnderstanding2011] :: digital literary typology

anti-essentialist :: [@osullivanDigitalPoetics2016] :: survey of aesthetic properties

philosophical :: [@durityStrugglingNewMedia2011] :: conservatism

philosophical :: $\ref{On Hybrid Forms}$ :: lexical automata, semetext

Common characteristics? From-the-text as opposed to theory-led?

Taking our cue from informatics we could if we so wished analyse works of electronic literature using the theoretical foundation of computer science: finite automata. What is being suggested here is not the theoretical analysis of the electronic arts through the lens of the models of the Turing machine or alternately the lambda calculus of the type-theoretic variety or not. Rather, what is being suggested is replacing the symbol with the lexeme thus alighting on the idea of lexical automata. Semiotically speaking, finite automata operate on sets of unrestricted symbols whereas surely it must be agreed that literary works are lexically constrained therefore electronic literary works must be in the final analysis characterised by lexical automata. This work is an attempt to show how the analysis of electronic literary works so understood and explored yields fruitful results.

What is meant by hybrid writing forms? Precisely this; the blending of unstructured and structured data. A word of caution: unstructured here does not mean devoid of all structure, it means yet-to-structured – this may appear to be a pedantic stipulation but it is one that will serve us well. Natural language here is taken to be unstructured. This is in contrast to the linked records and fields of structured data – what we know to be true, digitally represented. Not all literary works are works of fiction, thus at some level we will need to understand what it means to blend unstructured and structured data. Surprisingly little work has been carried out in this regard, notwithstanding the exemplar studies of [@erikssonSemanticDocumentApproachCombining2007] and [@nesicSemanticDocumentArchitecture2010]. The core idea here is to somehow combine semantic web technologies with the text. Each of these studies (and others) examines a variant of that. Building atop the base layer of Peircean semiotics is the most natural way for carrying out this task I propose. The resultant mixture is something I call semetext, it can be viewed if one wants as an extension of hypertext; indeed, explicit in the wider literature, though not acted upon, is the recommendation that if we replace the hyperlink with with something that carries typed information [@lewisSignificanceLinking1999] and [@gunsTracingOriginsSemantic2013] and something like what is being suggested here would emerge.

## Reflect, Inflect, Genuflect

Philosophy when it is dressed in literary garb is a genre of literature. It is a matter of debate how much of the activity of philosophy is literary practice, how much is verbal discourse, how much is time spent in the armchair, how much is spent in the conceptual laboratory, how much is spent changing the world, and so on. Is philosophy in the communication or in the cogitation? Or both?!

Suffice to say, the immediate conceptual residue of philosophical activity (at least before the onset of modern media) is a large body of pretty homogeneous literary work. This body of work shares a common heritage with the rest of literature that is inextricably woven together and which cannot be unpicked like a loose thread from a garment.

Artists as a group tend to be, by definition, more interested in the medium than the message than other communicators. This is such an obvious statement it is almost embarrassing to spell it out. It stands to reason then that vanguard explorations in new media will be led by artists. However, artists are not always their best critics. Undoubtedly they lack the dispassionate and disinterested gaze that leisurely reflection and outsider status brings. Artists have too much skin in the game. Sorry, sorry, such blanket statements will not do. Some artists like to document their explorations, sometimes in a diary, sometimes embedded in the work itself, sometimes over coffee and cigarettes in Parisian cafés. That is about the extent of it.

Elliptical though the point may be there is a point. Look to the literary criticism of electronic literature to see where philosophy will over time be heading – the same, of course, holds true for every other discipline. Our division again:

Content plane – philosophy of art and aesthetics is incrementally having its subject-matter altered to to countenance new forms of literary and artistic production: digital art, e-lit, e-poetry, and so on. Your average digital humanist will be well aware of the terrain I speak, your average philosopher not so much I imagine.

Expression plane – as alluded to in the previous chapter new, provocative and challenging literary and not so literary forms are either upon us or await us just over the formal event horizon.

A first service that can be done for philosophy is to bring it up to speed on what is out there. That is why we consider here two critical strands, the division I have made is somewhat arbitrary and contingent on personal relations. That being said I think it is a useful division and not altogether arbitrary, the first being top-down analysis, the second being bottom-up synthesis. These are but a sampling, the daunting knowledge base ELMCIP details 196 significant book-length monographs related to the field of electronic literature1.

1) embarrassingly brief and quick overview

2) philosophy must grapple with this body of work and its criticism

3) I propose YAMOEL (Yet Another Model of Electronic Literature) …

## Deterministic Lexical Automata

Taking our cue from informatics we could if we so wished analyse works of electronic literature using the theoretical foundation of computer science: finite automata. What is being suggested here is not the theoretical analysis of the electronic arts through the lens of the models of the Turing machine or alternately the lambda calculus of the type-theoretic variety or not. Rather, what is being suggested is replacing the symbol with the lexeme thus alighting on the idea of lexical automata. Semiotically speaking, finite automata operate on sets of unrestricted symbols whereas surely it must be agreed that literary works are lexically constrained therefore electronic literary works must be in the final analysis characterised by lexical automata. This work is an attempt to show how the analysis of electronic literary works so understood and explored yields fruitful results.

A short detour for reasons that shall become clear.

If you were to hold the opinion that the humanities represent the only area of academia that has as yet come to properly grasp the implications and work through the consequences of computing and computation you would be incorrect. One of the repeated moves in this dissertation is the pushing back against the idea that computers have arrived late to the humanities (in the methodological sense) because humanists are somehow less cognisant of the profound changes the impact must entail. The contrary is asserted here; it is precisely because humanists are very much aware of the implications and consequences that the arrival of computers has been relatively late.

Couple this with the inherent, dare I say it, chauvinism of the formal sciences we have reached the topsy-turvy upside-down place where the tail is wagging the dog. That is not to say that the arts and humanities do not have their own built-in chauvinistic tendencies. Of course they do. That is why the struggle for the soul of academia is so protracted, so entrenched, and why it is so difficult to balance the tension. And that is before the culture wars are added to the mix complicating divisions.

Why point this out? One might get the impression that the arts and humanities are in a relative state of crisis, disarray, and waywardness. (By the way, because philosophy is not treated as the neutral space that it is these divisions have also found their way into philosophy to the detriment of the discipline.) The study of computers and computing within academia suffers from its fair share of epistemological problems most of which go unacknowledged.

List the crisis, disarray, and waywardness of comp. sci.

software eng. versus info tech. versus ICT versus computer applications versus computer science versus informatics

Aww, my reasons failed to become clear.

___

## Try Try Again

Understanding Comics by Scott McCloud [@mccloudUnderstandingComicsInvisible1994] succeeds to the same degree that Unflattening by Nick Sousanis (Sousanis 2015) fails. Understanding Comics is an exploration of form2 from within the form itself. As a work of criticism it stands shoulder-to-shoulder alongside such greats like Culture and Anarchy by Matthew Arnold, Seven Types of Ambiguity by William Empson, Against Interpretation by Susan Sontag, and S/Z by Roland Barthes. You may think your author exaggerates. I assure you, I do not. Unflattening takes the mantra “show, don't tell” to its logical extreme and reflects upon the logocentrism of the humanities – stir in a spoonful of “let's shake up the oppressive orthodoxy” and you have got a dish to potentially tickle the taste buds. It does so, again, unorthodoxly, in graphic novel format.

It is unfair to some extent to compare the two works because the two share only the merest of superficial aims, but then again it is fair to compare the two works because the two share only the merest of superficial means.

Let me explain.

As a genre3 McCloud makes the case that comics (he insists on this term, not the – as he sees it – pretentious term graphic novel) are seen as the ugly step child of literature and could never be taken to be proper literature. The idea is that kids read comics; this obviously rankles. After a run-through of the evolution of the genre, followed by a convincing analysis of the genre (which amounts to “the whole is greater than the sum of its parts” – which makes the whole thing sound a lot more lame and trite than it in actual fact is) the reader is left in no doubt that we only await the arrival of a comic book Shakespeare to secure the genre's rightful place in the echelon of higher art-forms.

The object under criticism in Unflattening is the dissertation. Why? The tyranny of the symbolic. Will you not stick a flower down the barrel of that symbolic weapon and give iconicity a chance? It is clear the McCloud's work has left as much of an indelible impression on Sousanis as it has on this author. The problems in this case here are manifold. First, and I hesitate at the threshold because to judge harshly invites harsh judgment, Understanding Comics is both a great work of art as well as being a great work of literary criticism. It succeeds in inventing a genre that has been in existence for centuries, itself no mean feat. Unflattening may be many things but a great work of art it is not. Second, is that Unflattening is derivative. It is derivative of its clear predecessor and fails to acknowledge in full its critical lineage from Plato to Derrida with many points in between. Third, it is a missed opportunity. Yes, the dissertation is ripe for reinvention but embracing the comic book format is not how that reinvention is going to take place.

And here Understanding Comics and Unflattening have something in common. At the risk of pointing out the obvious, not everyone can draw. Literacy, in the West at least since the birth of mass primary education4, is a near universal skill. No one is seriously suggesting that art class be mandatory where now it is optional. Whether the intuition that most people just cannot draw and that drawing is an innate talent is correct or not –  and your intuition is as good as mine – there is no movement underway to make drawing (or pictorial literacy) a universal skill through mandatory schooling.

There have been, however, many calls around the globe to make digital literacy a core skill. There is the understanding of our present moment in history and that the future is only going to get even more digital and that we need to prepare the youth of the world for this computationally ubiquitous landscape – if in the end the intention is only so that society has enough coders to realise its digital future and so that we don't up creating an underclass of digital serfs trapped in a degrading gig economy. One could cite England5, Japan6, this EU comparison7, Australia8, Turkey9, and so on. One of the more remarkable moves in recent memory is the BBC micro:bit,
The BBC micro:bit is a pocket-sized codeable computer with motion detection, a built-in compass and Bluetooth technology, which was given free to every child in year 7 or equivalent across the UK in 2016.10
Convincing? I think so.

As was touched upon briefly in the previous chapter there are a couple of other works that explore this avenue. Two of note are the adventurous How Can You Love a Work If You Don't Know It? by Amanda Visconti (Visconti 2015) and the critical piece “Ph.D.s Embrace Alternative Dissertations. The Job Market May Not.” by Vimal Patel (Patel 2016) in the far-reaching Chronicle of Higher Education.

## So, Lexical Automata

Theory of Computation | Finite Automata Introduction

Finite Automata(FA) is the simplest machine to recognize patterns.

A Finite Automata consists of the following :

Q : Finite set of states.
∑ : set of Input Symbols.
q : Initial state.
F : set of Final States.
δ : Transition Function.

Formal specification of machine is
{ Q, ∑, q, F, δ }.

Some Important Points:

1. Every DFA is NFA but not vice versa.
2. Both NFA and DFA have same power and each NFA can be translated into a DFA.
3. There can be multiple final states in both DFA and NFA.
4. NFA is more of a theoretical concept.
5. DFA is used in Lexical Analysis in Compiler.

In the theory of computation, a branch of theoretical computer science, a deterministic finite automaton (DFA)—also known as deterministic finite acceptor (DFA), deterministic finite state machine (DFSM), or deterministic finite state automaton (DFSA)—is a finite-state machine that accepts or rejects strings of symbols and only produces a unique computation (or run) of the automaton for each input string. Deterministic refers to the uniqueness of the computation.

Theories of computation with more robust models, you'll recall from the previous chapter the axis representing the power of expression.

   Hierarchy   Grammars                     Languages                    Abstract machines
   ---------   -----------------            ----------------------       -----------------
   Type-0      Unrestricted                 Recursively enumerable       Turing machine
     —         (no common name)             Decidable                    Decider
   Type-1      Context-sensitive            Context-sensitive            Linear-bounded
     —
   Type-2      Context-free                 Context-free                 Context-free
     —         Deterministic context-free   Deterministic context-free   Deterministic pushdown
     —         Visibly pushdown             Visibly pushdown             Nested word (automata)
   Type-3      Regular                      Regular                      Finite (automata)
     —         —                            Star-free                    Counter-free (with aperiodic finite monoid)
     —         Non-recursive                Finite                       Acyclic finite

[@TemplateFormalLanguages2019]

The idea here is to not permit the power of type-0, let us start with what we've got and deform it ever so slightly using machine of type-3 expressivity.

That's basically it in a nutshell folks. To reiterate: the idea is that literary form is not deformed thoroughly, it is augmented methodically and in piecewise fashion. Measure matters. Take a work like The Ode Less Travelled 11 by Stephen Fry which spends 200 pages going into the nuts and bolts of verse: trochee, anapest, … To a casual observer this is as much engineering as it is art. Among artists and critics alike in this emerging field there is a distinct playing fast and loose with certainly what conventional poetic forms are, whatever about the wider and broader literary church. Probably this point has been made many times before. This is not my wheelhouse. My analysis and argument does not stand and fall on this point alone.

Am progressing beyond an essay written in 2011, never published.

There is also the matter of which level of expressivity, complexity, power, call it what you will, at which natural language is.

By 1985, several researchers in descriptive and mathematical linguistics had provided evidence against the hypothesis that the syntactic structure of natural language can be adequately described by context-free grammars.[1][2] At the same time, the step to the next level of the Chomsky hierarchy, to context-sensitive grammars, appeared both unnecessary and undesirable. In an attempt to pinpoint the exact formal power required for the adequate description of natural language syntax, Aravind Joshi characterized "grammars (and associated languages) that are only slightly more powerful than context-free grammars (context-free languages)". (Joshi 1985) He called these grammars mildly context-sensitive grammars and the associated languages mildly context-sensitive languages. (Wikipedia)
Why would one bolt on something even more expressive? That way open-ended madness lies. (Which coincidentally is a new media poetic work deserving of praise and admiration but not the rubric of poetry).

## Semetext: Semantic Document

An Idea Whose Time Has Come
This is an idea that goes back to 2007. What if, the idea goes, one were to combine unstructured data and structured data in the one entity? For our purposes here, structured data means data stored as records or graphs where the domain information has been broken into individual pieces of data: entities, attributes, and their relations–complete with their attendant types. Unstructured data is raw information like an image or a document, information that requires a human to interpret it. With the semantic web it becomes possible (and natural even) to imagine a hybrid entity, one that combines structured and unstructured information. Of course, as with all things, the devil is in the details. [@erikssonSemanticDocumentApproachCombining2007] is the oldest mention I can find–so the idea is a full decade old. Eriksson calls structured data ontologies and unstructured data documents. The use of the term ontology for this type of thing is quite solidified in computer science, it is not to be confused with but is related to the ontological considerations of philosophers. The more abstract (or fundamental) groups of concepts are call upper-level ontologies–the fundamental groups in semantic web land are called vocabularies and the non-fundamental domains are called data sets, from this one gets the terms: linked-open vocabularies and linked-open data. Confusingly controlled vocabularies are used to talk about things up and down the conceptual hierarchy.

Anyway, that is why Eriksson talks of combining documents and ontologies, in the conclusion he states, “We argue that ontologies and documents should be linked closely because they are two different but interrelated views of the same body of knowledge. An ontology without appropriate documentation is difficult to understand and review. Likewise, a document without sufficient metalevel descriptions is not equipped for automated reasoning and search. The semantic-document approach described here fuses the widely used PDF format with the popular ontology formats RDF/RDFS and OWL. Appropriate tool support is essential for the creation and processing of semantic documents. The extended Protégé allows developers to annotate PDF documents with RDF/RDFS and OWL statements in a powerful way.”
RDF/RDFS and OWL are fundamental to the semantic web. RDF stands for resource description framework. Everything in semantic web land is described using triples: subject, predicate, object–each of these is an online resource, hence resource description framework. Triples are stored in databases called triplestores. Clearly one must choose a document format to attach your triples to and come up with some kind of mechanism to link information in the unstructured part to triples in the structured part and the reverse. Online resources are accessed via URLs–universal resource locators in both the web and semantic web–this is their common feature.
Even older (Tazzoli, Castagna, and Campanini 2004) was the idea to combined structured data with the unstructured data of wikis by embedding triples in wiki pages. The semantic web has kick-started the convention of using the prefix semantic- to talk about semantic web influenced technology. So semantic-document and semantic-wiki.
A web page has at its heart what is called a DOM (document object model). (Nešić 2010b) in an article in Web 2.0 & Semantic Web and the same author (Nešić 2010a) again in his dissertation on this topic proposes a complex semantic document model. Logically I believe by analogy it should be called the semantic document object model. Given that the DOM describes web documents using hypertext, and hyperlinks I propose that a standardised SDOM describes semantic documents using semetext, and semelinks. Nešić calls the individual (unstructured) elements of a document content units in his article and document units in his dissertation. Here is his definition from 2009,
“A  Semantic  Document  is  a  uniquely  identified  and  semantically  annotated composite resource. It is built of smaller resources (CUs), which can be either composite or atomic and which are also uniquely identified and semantically annotated. Each document CU is characterized by its content (data) and knowledge, which are represented in a form understandable to both humans and machines. CUs can be put in different kinds of relationships with other, uniquely identified resources (e.g., other CUs, Web Pages, people, institutions, etc.). Hierarchical and navigational relationships among CUs are used to define document logical structure.”
I have been calling the individual units scholarly elements because I was fixated on the scholarly application of this technology and to differentiate them from the elements of a web page.
(Ideally a web browser should not initially be the tool of choice because browsers are not semantic web aware. Seme (Eco 1986, pg. 22) recalls semantic and sem(e)iotic and seme in linguistics is the smallest unit of meaning and invokes morpheme, lexeme, grapheme, and so on.)
KnowledgeStore (tag line: “Scalable storage for text and RDF data”) (Corcoglioniti et al. 2015) is an EU funded project which aims to manipulate structured and unstructured data and is prior art. Their overview reads,
“Despite the widespread diffusion of structured data sources and the public acclaim of the Linked Open Data initiative, a preponderant amount of information remains nowadays available only in unstructured form, both on the Web and within organizations. While different in form, structured and unstructured contents speak about the very same entities of the world, their properties and relations; still, frameworks for their seamless integration are lacking. The NewsReader KnowledgeStore is a scalable, fault-tolerant, and Semantic Web grounded storage system to jointly store, manage, retrieve, and semantically query, both structured and unstructured data. The KnowledgeStore plays a central role in the NewsReader EU project: it stores all contents that have to be processed and produced in order to extract knowledge from news, and it provides a shared data space through which NewsReader components cooperate.”
Apart from its focus on existing unstructured data rather than unstructured data authored on the spot this all sounds very much like the semantic-document idea and indeed the project's published articles reference Eriksson's work directly. They use different terminology again to talk about the semantic document object model–because they do not use the expression semantic document themselves–which is (sigh) core data model ontology (Rospocher, Serafini, and Corcoglioniti 2014).
Two very important and related questions at this juncture. Why am I proposing a yet another model and why has this hybrid format not taken off?
I'll tackle the second part first. No-one creates concepts ex nihilo. Concepts derive from other concepts and only make sense in relation to other concepts. Therefore one needs a critical mass of knowledge bases online using semantic web vocabularies and with sufficient depth and complexity. This is something that takes time.  Wikidata, the structured (semantic-) version of Wikipedia has only gotten into its stride in the last few years. Even if I had a tool in 2007 to create semantic documents I would have had nothing to link to. Also, which concrete document format to piggy back on? Eriksson suggests PDF, Tazzoli et al. suggest augmenting the wiki format (there are many), Eriksson suggests augmenting PDFs. In contrast Nešić suggests a general model which is a better step but then tries to capture every type of feature that documents contain: paragraphs, images, lists, headings, and so on.
When Tim Berners-Lee came up with the web he came up with a very simple document object model and a very simple textual implementation HTML (hypertext markup language) that was derived from SGML. I propose that STML (semetext markup language) be an enriched version of markdown because that will allow authors to use it directly–one must be able to author the unstructured text relatively unhindered much as one does when one uses Emacs or Vim or Notepad or LibreOffice Writer or Microsoft Office etc. This tool will fail if it feels like using a website which is another reason not to develop it in a web browser at least initially.
(Scholarly) Elements
Peirce has shown us that semiotically speaking there are but three elements12: symbol, icon, index (many many places but see “On the Algebra of Logic: A Contribution to the Philosophy of Notation” in The Essential Peirce: Selected Philosophical Writings, Volume 1  (Peirce 1998)). Symbolic signs represent their objects via convention–natural language is the prototypical example. icons represent their objects by resembling them as photographs do to pick a visual example, indexes point to their object as a finger does. This suggests that semantic documents ought to be composed of combinations of these three elements. (From playing around with the idea in my mind the notion of a placeholder is also necessary.)
Symbol. It makes sense to use the lightweight text format of markdown for semantic documents and extend it to make use of semantic links. This is described in the technical section at the start of this document. The editor that the author is using draws on a triplestore that is used and reused. Every time a document is saved only those triples that are connected with the document (a subset of the triples in the triplestore) are saved to a file ending with the extension .seme so that documents can be sent between people. In all likelihood a .seme file will be a compressed archive of all the elements in the document plus a list of the triples in one of the serialization formats of the semantic web such as RDF/XML or Turtle and (I'm unsure about this part) some index file or manifest file which provides an element to resource mappings and metadata–the reason I am unsure is because the triples can be used for this purpose seeing as how they can be used to represent any information but…
Icon. Luckily enough for our purposes most media formats have sections to allow arbitrary metadata. There is even a standard promoted by Adobe called XMP that takes advantage of this,
“Adobe’s Extensible Metadata Platform (XMP) is a file labeling technology that lets you embed metadata into files themselves during the content creation process. With an XMP enabled application, your workgroup can capture meaningful information about a project (such as titles and descriptions, searchable keywords, and up-to-date author and copyright information) in a format that is easily understood by your team as well as by software applications, hardware devices, and even file formats. Best of all, as team members modify files and assets, they can edit and update the metadata in real time during the workflow.”
Most importantly XMP supports semantic web vocabularies out of the box. A chief design requirement is to reuse as many standards as possible and integrate in as simple a fashion as possible.
Index. Finally, to round off the elements, all that is needed then is for some citation format that supports semantic web technologies.
Conceptual Analysis
Semetext is viewed as a platform or ecosystem for conceptual analysis.
Concept and Mind Mapping Software
file formats
Namespaces
Consider the Wikidata item for what in English is called a “human”. That resource lives here: [Entity Q5](http://www.wikidata.org/entity/Q5) Now every item in Wikidata gets its own unique code but they all share the same resource namespace [Entity](http://www.wikidata.org/entity/) which makes sense when you think about it. A vocabulary or data set only needs one namespace but some are logically divided into a number of namespaces. One refers to resources using angle brackets like so: <http://www.wikidata.org/entity/Q5> and in certain places one frequently uses prefixes to avoid repetition like so: ```<wd:Q5>```
Semetext introduces two namespaces. The first namespace corresponds to the vocabulary of the SDOM (the semantic document object model as you remember) and the second namespace corresponds to the conceptual space of the local triplestore. The first namespace (let us use the prefix seme: as no domain has been chosen as yet) is public and common to all semetext documents while the second namespace (let us use the prefix local: as the exact mechanism is undecided) is private and unique when exported so that as documents are shared agents–being a shorthand for people and bots–will be able to race concepts back to an origin and be able to recognise when two concepts “in the wild ” are equivalent.
Contexts
 There is a deep connection between named graphs, Peirce's notion of the interpretant, states-of-affairs, contexts, and conceptual personae. All of these are different ways of saying that meaning is context-dependent, that it is depends on the agent doing the meaning-making and the state of mind of the agent at that time. Nothing more, nothing less. This insight relies on the observation that when a sign and an object are brought together in an act of signification to use Peirce's terminology they will not necessarily be brought together in the same way by two different agents and not even the same agent at different times. It is what gives us space for interpretation. This is not to say that anything goes–what advocates of radical interpretation forget here is that intersubjectivity sets limits on the act of interpretation.
This also allows for the notion of an early Wittgenstein and a late Wittgenstein–one person, two conceptual personae. It allows for the adjectives Platonic and Kantian. Right at the heart of the semantic web is the notion of quads rather than triples. The semantic web recognises that a triple is created within a context and allows you to record that context. Similarly type theory makes use of contexts where formal logic tends not to because formal logic is concerned with universals and with logical constants, because of this one does not need context, that is in part the tyranny and beauty of formal logic. Deleuze and Guattari are at pains to point out that philosophy is performed (can think of no better word though that word seems off) by conceptual personae.
Contexts are nested hierarchically, the top (or root to use a computer science term) context being the global context.
Reframe
Reframe is a reference implementation. Like all the other software projects relating to my dissertation it is meant to be first and foremost a proof of concept. It is a simple terminal-based editor. My hope is that many individuals and organisations will create implementations so that Reframe will be but one of many and that semetext will evolve in coordination from the best ideas that flow from these projects. Anyone aware of AI research will recognise that the concept of a frame is borrowed from philosophy of mind and AI inspired theories of cognition. Then, as I am advocating a shift in perspective from the traditional to the digital, the notion of reframing is apt. Lastly, there are echoes of Heidegger's Gestell (framing, framework) from his The Question Concerning Technology (Heidegger 2010).
SDOM–Semantic Document Object Model
The SDOM is a formal description using the language of the semantic web of my take on semantic documents. Thus, there are three fundamental things: entities, properties, contexts. Entities are noun-like things, properties are verb-like things, contexts are things that have state. This is modelled following the Wikidata data model which has two fundamental things: items and properties. This is a recognition that the semantic web triple [subject, predicate, object] format says,  “who did what to whom”. Let us not get hung up on things like that a verb could be the subject or object of a statement for the moment, nor let us get hung up on the fact that these only represent two of our parts of speech. What we are taking from linguistics is the idea of open and closed classes.  Closed classes (those which comprise function words) are the structural or grammatical components of a language and open classes (those which comprise lexical words) are the substantial components.

## Lessons From Elsewhere

Bret Victor, Jupyter Notebooks, …
_
